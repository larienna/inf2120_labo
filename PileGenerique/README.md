# INF2120 Laboratoires
par Eric Pietrocupo

## Exercice: Piles Génériques

Dans cet exercice, vous devez faire 2 implémentations différentes de la même interface de pile. La même fonction de tests sera utilisée pour valider le fonctionnement de chacune des piles. Les deux doivent donner le même résultat. Utilisez le programme principal suivant pour tester vos piles.

[Programme de test](src/pilegenerique/PileGenerique.java)

Vos deux piles doivent implémenter l'interface suivante:

[Interface de pile](src/pilegenerique/Pile.java)

## Pile Arraylist

Votre première pile doit implémenter celle-ci avec un `ArrayList`. Ils sont des tableaux dynamiques, donc vous n'avez pas à gérer l'espace comme un tableau. Voici l'entête cette pile:

~~~java
public class PileArraylist<T> implements Pile<T>{
   private ArrayList<T> list;

   //TODO ...
}
~~~

Vous devez implémenter toutes les fonctions de l'interface `Pile` ainsi que la méthode `toString()`.

__Note__: Pour votre information, on peut difficilement faire un tableau générique tel qu'expliqué dans l'article suivant:

[Creating a Generic Array in Java](https://www.baeldung.com/java-generic-array)

## Pile Chaînée

Votre deuxième pile devra faire l'implémentation via une liste chaînée. On utilisera des noeuds qui contiendront un élément ainsi qu'une référence vers le noeud précédant. On aura une référence sur la tête de la pile pour pouvoir ajouter et retirer des éléments. Voici l'entête de cette nouvelle pile avec la classe interne `Noeud`:

~~~java
public class PileChaine<T> implements Pile<T> {
   private static class Noeud<T>
   {  public T element;
      public Noeud<T> precedant;

      public Noeud(T element)
      {  this.element = element;
         this.precedant = null;
      }
   }
   
   private Noeud<T> tete;
   private int nbElement;

	//TODO ...
~~~

Vous devez d'abord implémenter toutes les fonctions de l'interface `Pile` et ensuite vous devez implémenter la fonction `toString()` __récursivement__. Vous devrez utiliser un `StringBuilder` pour construire votre chaîne de caractères dans une fonction récursive séparée puisque `toString()` elle-même n'est pas récursive. L'idée est de partir de la tête pile, de descendre jusqu'à la base, et d'afficher les éléments au retour de la récursion en remontant la pile.

## Exemple de sortie

Voici un exemple d'exécution, les deux piles affichent le même résultat lorsqu'elles sont testées par le programme principal.

~~~
_____ Test de la pile Arraylist _____
[12]
[12, 25]
[12, 25, 48]
On retire l'element: 48
[12, 25]
[12, 25, 18]
[12, 25, 18, 54]
On retire l'element: 54
[12, 25, 18]
On retire l'element: 18
[12, 25]
On retire l'element: 25
[12]
[12, 5]
On retire l'element: 5
[12]
On retire l'element: 12
[]
On retire l'element: null
[]
Est-ce que la pile est vide ?: true
_____ Test de la pile Chainee _____
[12]
[12, 25]
[12, 25, 48]
On retire l'element: 48
[12, 25]
[12, 25, 18]
[12, 25, 18, 54]
On retire l'element: 54
[12, 25, 18]
On retire l'element: 18
[12, 25]
On retire l'element: 25
[12]
[12, 5]
On retire l'element: 5
[12]
On retire l'element: 12
[]
On retire l'element: null
[]
Est-ce que la pile est vide ?: true
~~~

## Solutions

[Pile Arraylist](src/pilegenerique/PileArraylist.java)

[Pile Chainée](src/pilegenerique/PileChaine.java)

