/*
 * Copyright 2023 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pilegenerique;

/**
 * Interface qui permet l'implementation d'une pile avec divers implementations
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 * @param <T> Type de donnee a empiler
 */
public interface Pile<T> {
   /**Retourne true si la pile est vide*/
   public boolean vide();
   /**Empile element sur le dessus de la pile*/
   public void push( T element );
   /**Retire un element sur le dessus de la pile, retourne null en cas d'erreur*/
   public T pop();   
}
