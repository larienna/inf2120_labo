/*
 * Copyright 2023 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pilegenerique;

/**
 *
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class PileChaine<T> implements Pile<T> {
   private static class Noeud<T>
   {  public T element;
      public Noeud<T> precedant;

      public Noeud(T element)
      {  this.element = element;
         this.precedant = null;
      }
   }
   
   private Noeud<T> tete;
   private int nbElement;
   
   PileChaine ()
   {  tete = null;
      nbElement = 0;
   }
   
   @Override
   public boolean vide()
   {  return (nbElement == 0);      
   }

   @Override
   public void push(T element)
   {  Noeud<T> n = new Noeud(element);
      n.precedant = tete;
      tete = n;
      nbElement++;
   }

   @Override
   public T pop()
   {  if ( tete == null) return null;
   
      Noeud<T> n = tete; 
      tete = n.precedant;
      nbElement--;
      return n.element;
   }

   @Override
   public String toString ()
   {  StringBuilder strbld = new StringBuilder();
      strbld.append("[");
      toStringRecursif ( strbld, tete );
      strbld.append("]");
      return strbld.toString();
   }
   
   private void toStringRecursif ( StringBuilder strbld, Noeud<T> n )
   {  // cas de base
      if ( n == null) return;
      //On creuse d'abord avant d'afficher
      toStringRecursif(strbld, n.precedant);
      //Ensuite on affiche en sortant de la recursion
      strbld.append(n.element.toString());
      if ( n != tete ) strbld.append(", ");
   }
}
