/*
 * Copyright 2023 Eric Pietrocupo <ericp@lariennalibrary.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pilegenerique;

import java.util.ArrayList;

/**
 *
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class PileArraylist<T> implements Pile<T>{
   private ArrayList<T> list;
   
   public PileArraylist ( )
   {  list = new ArrayList<>();      
   }
   
   @Override
   public boolean vide()
   {  return list.isEmpty();      
   }

   @Override
   public void push(T element)
   {  list.add(element);      
   }

   @Override
   public T pop()
   {  if ( list.isEmpty() == true) return null;
      return list.remove(list.size()-1);      
   }
   
   @Override
   public String toString ()
   {  return list.toString();
   }
}
