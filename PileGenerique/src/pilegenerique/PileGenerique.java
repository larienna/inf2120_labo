/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pilegenerique;

/**
 *
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class PileGenerique
{

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args)
   {  Pile<Integer> pileA = new PileArraylist<>();
      Pile<Integer> pileB = new PileChaine<>();
      
      System.out.println("_____ Test de la pile Arraylist _____");
      testPile(pileA);
      System.out.println("_____ Test de la pile Chainee _____");
      testPile(pileB);
   }
   
   public static void testPile( Pile<Integer> pile)
   {  pile.push(12);
      System.out.println(pile);
      pile.push(25);
      System.out.println(pile);
      pile.push(48);
      System.out.println(pile);
      System.out.println("On retire l'element: " + pile.pop());
      System.out.println(pile);
      pile.push(18);
      System.out.println(pile);
      pile.push(54);
      System.out.println(pile);      
      System.out.println("On retire l'element: " + pile.pop());
      System.out.println(pile);
      System.out.println("On retire l'element: " + pile.pop());
      System.out.println(pile);
      System.out.println("On retire l'element: " + pile.pop());
      System.out.println(pile);
      pile.push(5);
      System.out.println(pile);      
      System.out.println("On retire l'element: " + pile.pop());
      System.out.println(pile);      
      System.out.println("On retire l'element: " + pile.pop());
      System.out.println(pile);
      System.out.println("On retire l'element: " + pile.pop());
      System.out.println(pile);
      System.out.println("Est-ce que la pile est vide ?: " + pile.vide());
   }
   
}
