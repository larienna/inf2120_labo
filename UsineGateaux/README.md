# INF2120 Laboratoires
par Eric Pietrocupo

## Exercice: l'usine à gâteaux

### Objectifs

Cet exercice est un jeu de programmation qui consiste à construire une liste précise de gâteau en moins de temps possible. Ces gâteaux sont construits en assignant des références de gâteaux à différentes stations dans le but de les remplir, cuire et glacer. Ils sont ensuite chargés dans un camion pour les afficher à l'écran à la fermeture de l'usine. Voici un exemple d'exécution:

~~~
Camion plein, on ferme l'usine
___Fermeture de l'usine___
Temps écoulé : 30
No Pâte       Glacage    Cuisson Bien_fait
01 Vanille    Vanille    1       true
02 Vanille    Chocolat   1       true
03 Orange     Chocolat   1       true
04 Chocolat   Chocolat   1       true
05 Fraise     Fraise     1       true
06 Vanille    Orange     1       true
07 Chocolat   Vanille    1       true
08 Vanille    Vanille    1       true
09 Chocolat   Fraise     1       true
10 Fraise     Chocolat   1       true
~~~

Il est possible d'implémenter moins de 10 gâteaux et de fermer l'usine plus tôt pour savoir si votre solution est valide jusqu'à maintenant. On ne cherche pas à faire une solution générique, seulement résoudre la liste ci-dessus. Comparez votre pointage avec les autres étudiants et essayer de faire mieux.

Il faudra s'assurer de ne pas dupliquer les gâteaux par erreur, ou de les perdre dans le ramasse-miettes. Il faut donc bien gérer ses références.

### Validation de la solution

Vous devez valider la solution vous-même. Le résultat devrait être le même que la liste ci-dessus.

* Les gâteaux doivent être en ordre de numéro de 01 à 10
* Les saveurs doivent correspondre pour chaque gâteau à la liste ci-dessus.
* Le niveau de cuisson doit être 1
* `Bien_fait` doit afficher "true", ceci indique que toutes les étapes on été faites dans le bon ordre une seule fois.

## Règles du jeu

### Stations et construction de gâteaux

Voici le processus de construction de gâteaux. Les étapes sont les suivantes et doivent être accomplies dans l'ordre une seule fois:

~~~
[Remplissage]->[Cuisson]->[Glaçage]
~~~

Une fois les 3 étapes terminées, le gâteau peut être mis dans le camion. Pour chacune des étapes, il y a une machine différente qui peut accueillir plusieurs gâteaux:

* 2 Stations de remplissage : rempli les moules vide avec la saveur sélectionnée. 
* 3 Stations de cuisson: augmente le niveau de cuisson des gâteaux.
* 2 Stations de glaçage: glace les gâteaux avec la saveur sélectionnée.

Les stations de remplissage et de glaçage ont chacune leur saveur qui peut être choisie parmi la liste:

* Vanille
* Chocolat
* Fraise
* Orange

Lorsque qu'une machine est activée, les deux stations appliqueront la MÊME SAVEUR sur les deux gâteaux. Par exemple, si les deux stations de glaçage ont un gâteau et que la saveur sélectionnée est `CHOCOLAT`, les deux gâteaux seront glacées au chocolat lorsque les machines sont activées.

Donc il faut déplacer les gâteaux entre les diverses stations et activer les machines pour faire fonctionner celles-ci d'un seul coup. C'est à ce moment que le temps augment ce qui contribue à réduire votre score.

Une fois un gâteau prêt, on peut l'emboîter et le mettre dans le camion. Il faut que les gâteaux soient chargés en ordre numérique et il n'y a que 10 espaces dans le camion.

### Contraintes

* Vous ne pouvez que modifier la fonction main().
* Vous pouvez faire des sous-fonctions au besoin.
* Vous avez le droit de consulter le reste du code, mais vous ne pouvez le modifier.
* Il est légal d'avoir plusieurs références qui pointent vers le même gâteau.
* Vous ne pouvez qu'exécuter les fonctions mentionnées ci-dessous.

### Fonctions valides

Voici les fonctions que vous avez le droit d'utiliser:

~~~
Gateau.Gateau() /* Constructeur sans parametres seulement*/
UsineGateaux.UsineGateaux() /*Constructeur*/
UsineGateaux.activerMachines() /*Active toutes les machines*/
UsineGateaux.emboiterGateau() /*Place le gâteau dans le camion*/
UsineGateaux.fermer() /*Fermer l'usine plus tôt que prévue*/
~~~

## Comment utiliser le code

Le code source est disponible au lien ci-dessous. Télécharger les 4 fichiers et faites un nouveau projet.

[Code source](UsineGateaux/src)

Vous pouvez effacer le contenu du `main()` et mettre votre code à l'intérieur.

### Gateau

Il y a une classe `Gateau` que vous devez instancier à l'aide de l'opérateur `new` sans paramètre pour chaque gâteau à construire. Les autres fonctions de la classe ne peuvent être utilisés.

~~~java
public Gateau ()
~~~

### Saveur

Dans la classe `Saveur`, vous avez 4 constantes que vous devez utiliser pour assigner les différentes saveurs aux machines:

~~~java
public static final Saveur VANILLE = new Saveur("Vanille");
public static final Saveur CHOCOLAT = new Saveur("Chocolat");
public static final Saveur FRAISE = new Saveur("Fraise");
public static final Saveur ORANGE = new Saveur("Orange");
~~~


### UsineGateaux: attributs publics

Cette classe est celle que vous utiliserez le plus. Faites en une instance dans votre programme principal via le constructeur avant de l'utiliser. Il y a plusieurs attributs publics, dont les saveurs pour les machines de remplissage et de glaçage:

~~~java
public  Saveur saveurPate;
public  Saveur saveurGlacage;
~~~

Il suffit que d'assigner ces références à une des constantes de `Saveur` pour changer la saveur utilisée par de la machine. Ensuite, il y a des références publiques pour les différentes stations auxquelles vous allez assigner vos gâteaux:

~~~java
public  Gateau[] stationRemplissage;
public  Gateau[] stationCuisson;
public  Gateau[] stationGlacage;
~~~

Ce sont des tableaux de 2 ou 3 éléments tel que défini dans le constructeur de la classe:

~~~java
stationRemplissage = new Gateau[2];
stationCuisson = new Gateau[3];
stationGlacage = new Gateau[2];
~~~

### UsineGateaux: fonctions

Il y a 3 fonctions disponibles dans cette classe:

~~~java
public void activerMachines ()
public void emboiterGateau ( Gateau Gateau )
public void fermer ()
~~~

La fonction `activerMachine()` activera chacune des machines dans un ordre précis et incrémentera la variable de temps qui est votre pointage. La fonction `emboiterGateau()` fait une copie de votre gâteau et le met dans le camion. La fonction `fermer()` permet de fermer l'usine plus tôt et de consulter l'état de votre camion. L'usine ferme automatiquement lorsque le 10e gâteau est chargé dans le camion.

### Exemple de main

Voici un exemple de code qui crée un seul gâteau.

~~~
public static void main_exemple(){
      UsineGateaux ug = new UsineGateaux();
      Gateau g = new Gateau();
      ug.stationRemplissage[0] = g;
      ug.saveurPate = Saveur.CHOCOLAT;
	  ug.activerMachines();

      ug.stationCuisson[0] = g;
      ug.stationRemplissage[0] = null;
      ug.activerMachines();

      ug.stationGlacage[0] = g;
      ug.stationCuisson[0]= null;
      ug.saveurGlacage = Saveur.ORANGE;
      ug.activerMachines();
      
      ug.emboiterGateau (ug.stationGlacage[0]);
            
      ug.fermer();
}
~~~




