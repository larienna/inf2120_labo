/**
 * Simple classe qui aurait pu etre un type enumeration. Contient la liste de
 * toutes les saveurs sauvegardes sous forme de constantes. La seule chose qui
 * est important d'utiliser sont les constantes comme par exemple:
 * 
 * Saveur.VANILLE
 * Saveur.CHOCOLAT
 * etc.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */

public class Saveur
{   public static final Saveur VANILLE = new Saveur("Vanille");
    public static final Saveur CHOCOLAT = new Saveur("Chocolat");
    public static final Saveur FRAISE = new Saveur("Fraise");
    public static final Saveur ORANGE = new Saveur("Orange");

    private String nom;

    public Saveur ( String nom) {
      this.nom = nom;
    }

    public String toString(){
       return nom;
    }

}
