/**
 * Cette classe permet de construire des gateaux en assignant les references
 * de stations a des gateaux alloues avec 'new'. Ensuite on lance 
 * activerMachines pour construire les gateaux.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */

public class UsineGateaux
{  private  static final int TAILLE_CAMION = 10;

   /** Les differentes stations aux quelles vous pouvez assigner des gateaux.
    * Les attributs sont public pour pouvoir gongler avec les references.
   */
   public  Gateau[] stationRemplissage;
   public  Gateau[] stationCuisson;
   public  Gateau[] stationGlacage;
   public  Saveur saveurPate;
   public  Saveur saveurGlacage;

   private  int temps = 0;
   private  int positionCamion = 0;
   private  Gateau[] camion = new Gateau[TAILLE_CAMION];

   /**
    * Initialisation des valeurs de l'usine ainsi que la creation des differentes
    * stations.
    */
   public UsineGateaux (){
      stationRemplissage = new Gateau[2];
      stationCuisson = new Gateau[3];
      stationGlacage = new Gateau[2];      
      temps = 0;
      positionCamion = 0;
      camion = new Gateau[TAILLE_CAMION];
   }

   
   /**
    * Active chacune des machines dans un ordre precis. Toutes les possitions
    * de la machine sont actives. Le temps est incremente
    */
   public void activerMachines (){
      temps++;
      for ( Gateau g: stationRemplissage) if ( g != null) g.remplir(this);
      for ( Gateau g: stationCuisson) if (g !=null) g.cuire();
      for ( Gateau g: stationGlacage) if (g !=null) g.glacer(this);    
   }
   
   /**
    * On fait une copie du gateau passe ne parametre pour le mettre dans le
    * camion. Donc toutes les modifications subsequentes au gateau original
    * seront ignores
    * @param gateau a charger dans le camion
    */
   public void emboiterGateau ( Gateau gateau ){  
      if ( gateau != null ){
         Gateau g = new Gateau(gateau);
         camion[positionCamion] = g;
         positionCamion++;
      }
      if (positionCamion >= TAILLE_CAMION ){
         System.out.println("Camion plein, on ferme l'usine");
         fermer();
      }  
   }
 
   /**
    * Ferme l'usine et affice le contenu du camion avant de terminer le programme
    */
   public void fermer (){
      System.out.println("___Fermeture de l'usine___");
      System.out.println("Temps écoulé : " + temps );
      System.out.println("No Pâte       Glacage    Cuisson Bien_fait");
      
      for ( int i = 0 ; i < positionCamion ; i++ ){
         System.out.println( camion[i]);
      }
      System.exit(0);
   }
}

