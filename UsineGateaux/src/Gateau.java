/**
 * Classe que vous devez instancier pour faire des nouveaux gateaux, vous aurez
 * a conserver des references vers ces instances, mais vous ne pouvez pas 
 * interagir vous meme avec ces objet. C'est l'usine a gateau qui se chargera
 * de faire les modifications necessaires aux gateaux.
 * 
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */

public class Gateau
{  public static final int VIDE = 0;
   public static final int CRUE = 1;
   public static final int CUIT = 2;
   public static final int GLACE = 3;
   public static final int ERREUR = 4;

   private Saveur pate;
   private int cuisson;
   private Saveur glacage;
   private int etape;
   private int numero;

   /** Compteur de numero unique*/
   private static int prochainNumero = 1;

   /**
    * Constructeur pour faire des noueaux gateaux vides. Un numero unique
    * est assigne a chaque gateau.
    */
   public Gateau (){
      pate = null;
      cuisson = 0;
      glacage = null;
      etape = VIDE;
      numero = prochainNumero;
      prochainNumero++;
   }
   
   /**
    * Constructeur par copie
    * Cette fonction doit etre appelee par l'usine.
    * @param g gateau a copier
    */
   public Gateau( Gateau g ){
      pate = g.pate;
      cuisson = g.cuisson;
      glacage = g.glacage;
      etape = g.etape;
      numero = g.numero;
   }

   /**
    * Remempli le moule avec de la pate, met a jour le statut du gateau    * 
    * Cette fonction doit etre appelee par l'usine.
    * @param ug 
    */
   public void remplir( UsineGateaux ug){
      pate = ug.saveurPate;
      if ( etape == VIDE) etape = CRUE;
      else etape = ERREUR;
   }

   /**
    * Augmente le compteur de cuisson et met a jour le statut du gateau
    * Cette fonction doit etre appelee par l'usine.
    */
   public void cuire(){
      if ( etape != VIDE) cuisson++;
      if (etape == CRUE) etape = CUIT;
      else etape = ERREUR;
   }

   /**
    * Ajoute du glacage au gateau, met a jour le statut du gateau.
    * Cette fonction doit etre appelee par l'usine.
    * @param ug Usine a utiliser
    */
   public void glacer(UsineGateaux ug){
      glacage = ug.saveurGlacage;
      if (etape == CUIT) etape = GLACE;
      else etape = ERREUR;
   }

   @Override
   public String toString () {      
      return String.format("%02d %-10s %-10s %-7d %s", numero, pate, glacage, cuisson, (etape == GLACE));      
   }

}
