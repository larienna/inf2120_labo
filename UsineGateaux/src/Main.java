public class Main{
	public static void main(String args[]){
      main_commande();

	}

   /**
    * Un Exemple de commande pour un seul gateau   
   */
   
	public static void main_exemple(){
      UsineGateaux ug = new UsineGateaux();
      Gateau g = new Gateau();
      ug.stationRemplissage[0] = g;
      ug.saveurPate = Saveur.CHOCOLAT;
	   ug.activerMachines();

      ug.stationCuisson[0] = g;
      ug.stationRemplissage[0] = null;
      ug.activerMachines();

      ug.stationGlacage[0] = g;
      ug.stationCuisson[0]= null;
      ug.saveurGlacage = Saveur.ORANGE;
      ug.activerMachines();      
      
      ug.emboiterGateau (ug.stationGlacage[0]);   
            
      ug.fermer();
	}

   /**
    * Fait une commande complète de gateaux à titre de démonstration.
    * La solution n'est pas optimale, on peut faire la même commande en moins
    * de temps.
    */
	public static void main_commande(){
      UsineGateaux ug = new UsineGateaux();
      
      construire_gateau ( ug, Saveur.VANILLE, Saveur.VANILLE);
      construire_gateau ( ug, Saveur.VANILLE, Saveur.CHOCOLAT);
      construire_gateau ( ug, Saveur.ORANGE, Saveur.CHOCOLAT);
      construire_gateau ( ug, Saveur.CHOCOLAT, Saveur.CHOCOLAT);
      construire_gateau ( ug, Saveur.FRAISE, Saveur.FRAISE);
      construire_gateau ( ug, Saveur.VANILLE, Saveur.ORANGE);
      construire_gateau ( ug, Saveur.CHOCOLAT, Saveur.VANILLE);
      construire_gateau ( ug, Saveur.VANILLE, Saveur.VANILLE);
      construire_gateau ( ug, Saveur.CHOCOLAT, Saveur.FRAISE);
      construire_gateau ( ug, Saveur.FRAISE, Saveur.CHOCOLAT);
      
	}
   
   /**
    * Cette fonction cree des gateaux de facon securitaire, mais completement
    * innefficace au niveau du temps. Fait chacune des etapes une a une et met
    * le gateau dans le camion.
    * @param ug References vers une usine a gateau
    * @param pate Saveur a utiliser pour la pate
    * @param glacage Saveur a utiliser pour le glacage
    */
   public static void construire_gateau ( UsineGateaux ug, Saveur pate, Saveur glacage){
      Gateau g = new Gateau();
      ug.stationRemplissage[0] = g;
      ug.saveurPate = pate;
	   ug.activerMachines();

      ug.stationCuisson[0] = g;
      ug.stationRemplissage[0] = null;
      ug.activerMachines();

      ug.stationGlacage[0] = g;
      ug.stationCuisson[0]= null;
      ug.saveurGlacage = glacage;
      ug.activerMachines();
      ug.stationGlacage[0] = null;
      
      ug.emboiterGateau (g);    
   }
              
   
}
