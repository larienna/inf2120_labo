# INF2120 Laboratoires
par Eric Pietrocupo

## Exercice 1: Types dynamiques et statiques

### Question 1.1
Voici une liste déclaration de classe

~~~java
public class Animal ...
public class DomesticAnimal extends Animal ...
public class FarmAnimal extends DomesticAnimal...
public class HousePet extends DomesticAnimal...
public class Cow extends FarmAnimal ...
public class Goat extends FarmAnimal ...
public class DairyCow extends Cow ...
~~~

Faites le diagramme de classe avec les flèches d'héritages. 

### Question 1.2
Parmi les assignations de références ci-dessous:

~~~java
DairyCow dc = new FarmAnimal();
FarmAnimal fa = new Goat();
Cow c1 = new DomesticAnimal();
Cow c2 = new DairyCow();
DomesticAnimal dom = new HousePet();
~~~

Lesquels sont des assignations qui ne crée pas d'erreurs de compilation?

<details>
<summary>Solution: Assignations valides</summary>

~~~
FarmAnimal fa = new Goat();
Cow c2 = new DairyCow();
DomesticAnimal dom = new HousePet();
~~~

</details>

SOURCE: https://runestone.academy/ns/books/published/javajavajava/inheritance-exercises.html

## Exercice 2: Héritage: Appels de fonctions

Voici 2 définitions de classe:

~~~java
public class Parent {
   private String var = "Ananas";
   public String fonctionA (){ return "Banane"; }
   public static String fonctionB(){ return "Orange";}
   public String fonctionC( double a){ return "Bleuet";}
   public String fonctionD(){ return "Pomme";}
   public String fonctionE(){ return var;}
   public String fonctionF(){ return "Mangue";}
}
~~~

~~~java
public class Enfant extends Parent{
   private String var = "Carambole";
   public String fonctionA (){ return "Tomate"; }
   public static String fonctionB(){ return "Raisin";}
   public String fonctionC( int a){ return "Fraise";}
   public String fonctionD(){ return super.fonctionA();}
   public String fonctionE(){ return var;}
}
~~~

Quel sera le résultat affiché par le code suivant?:

~~~java
public static void main(String args[]){
      Parent p = new Parent();
      Parent pe = new Enfant();
      Enfant e = new Enfant();
                  
      System.out.println("F1  : " + p.fonctionA()  );
      System.out.println("F2  : " + pe.fonctionA()  );
      System.out.println("F3  : " + Parent.fonctionB()  );
      System.out.println("F4  : " + Enfant.fonctionB()  );
      System.out.println("F5  : " + pe.fonctionC(42)  );
      System.out.println("F6  : " + e.fonctionC(42)  );
      System.out.println("F7  : " + pe.fonctionC(25.3)  );
      System.out.println("F8  : " + e.fonctionC(25.3)  );
      System.out.println("F9  : " + p.fonctionD()  );
      System.out.println("F10 : " + pe.fonctionD()  );
      System.out.println("F11 : " + p.fonctionE()  );
      System.out.println("F12 : " + pe.fonctionE()  );
      System.out.println("F11 : " + p.fonctionF()  );
      System.out.println("F12 : " + e.fonctionF()  );
   }
~~~

<details>
<summary>Solution: Sortie à l'écran</summary>

~~~
F1  : Banane
F2  : Tomate
F3  : Orange
F4  : Raisin
F5  : Bleuet
F6  : Fraise
F7  : Bleuet
F8  : Bleuet
F9  : Pomme
F10 : Banane
F11 : Ananas
F12 : Carambole
F11 : Mangue
F12 : Mangue
~~~

</details>

SOURCES D'INSPIRATION: 

[Answers to Questions and Exercises: Inheritance](https://docs.oracle.com/javase/tutorial/java/IandI/QandE/inherit-answers.html)

[10 Inheritance Example Program in Java for Practice](https://www.scientecheasy.com/2019/01/java-inheritance-example-program.html/)

## Exercice 3: Énumerations

Convertissez ma classe `Saveur` dans le projet d'usine à gâteau en classe `Enum`.

[Code source](../UsineGateaux/src/Saveur.java)

La documentation de Oracle sur le fonctionnement des types `Enum`:

[Enum Types](https://docs.oracle.com/javase/tutorial/java/javaOO/enum.html)

