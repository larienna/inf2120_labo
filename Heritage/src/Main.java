

/**
 *
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class Main {
   
   public static void main(String args[]){         
      Parent p = new Parent();
      Parent pe = new Enfant();
      Enfant e = new Enfant();
                  
      System.out.println("F1  : " + p.fonctionA()  );
      System.out.println("F2  : " + pe.fonctionA()  );
      System.out.println("F3  : " + Parent.fonctionB()  );
      System.out.println("F4  : " + Enfant.fonctionB()  );
      System.out.println("F5  : " + pe.fonctionC(42)  );
      System.out.println("F6  : " + e.fonctionC(42)  );      
      System.out.println("F7  : " + pe.fonctionC(25.3)  );
      System.out.println("F8  : " + e.fonctionC(25.3)  );      
      System.out.println("F9  : " + p.fonctionD()  );
      System.out.println("F10 : " + pe.fonctionD()  );
      System.out.println("F11 : " + p.fonctionE()  );
      System.out.println("F12 : " + pe.fonctionE()  );
      System.out.println("F11 : " + p.fonctionF()  );
      System.out.println("F12 : " + e.fonctionF()  );      
   }
      
}
