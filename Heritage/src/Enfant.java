
/**
 *
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class Enfant extends Parent{
   private String var = "Carambole";
   public String fonctionA (){ return "Tomate"; }
   public static String fonctionB(){ return "Raisin";}
   public String fonctionC( int a){ return "Fraise";}
   public String fonctionD(){ return super.fonctionA();}
   public String fonctionE(){ return var;}   
}
