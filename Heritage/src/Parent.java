

/**
 *
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class Parent {
   private String var = "Ananas";
   public String fonctionA (){ return "Banane"; }
   public static String fonctionB(){ return "Orange";}
   public String fonctionC( double a){ return "Bleuet";}
   public String fonctionD(){ return "Pomme";}
   public String fonctionE(){ return var;}
   public String fonctionF(){ return "Mangue";}   
}
