# Introduction à l'analyse de processus

Dans le cadre des cours INF5151, INF4150 et possiblement AOT1110 (Le code de cours a changé), vous verrez comment analyser les processus d'affaires dans le but de les informatiser. Il existe différentes méthodes qui ont chacune leurs forces et leurs faiblesses. Elles créeront divers diagrammes.

* Les cas d'utilisation (Use cases)
* BPMN: Business Process Model and Notation
* Les personas
* Analyse hiérarchique de la tâche
* Cartographie de l’itinéraire utilisateur (Journey Maps)

Pour cet exercice, on utilisera une méthode simple qui sera suffisante pour construire vos travaux pratiques. L'objectif est de ne pas se perdre dans l'organisation des classes pour éviter divers problèmes comme la sur-abstraction. Donc je propose dans cet exercice une solution pratique et simple qui sera suffisante pour les besoins du cours.

Tout programme, peu importe le langage, la plate-forme et le processus d'affaire à analyser est composé de 2 choses: du code et des données. On fera donc 2 schémas pour modéliser chacun de ceux-ci. Je vais vous montrer un exemple concret pour modéliser la commande d'une pizza, et vous ferez la même chose avec des gâteaux ... pour rester dans le thème.

# Analyse du code

La première partie consiste à déterminer la liste des étapes nécessaires pour arriver à un résultat. Chacune des étapes peut être décomposée en sous-étapes indéfiniment. Cette structure correspond à la séquence d'appel de fonctions à partir du `main()`. Voici un exemple pour le processus de commande de pizza, ce modèle est inspiré de l'analyse hiérarchique de la tâche:

~~~
main()
  1. Prendre la commande
    1.1 Répondre aux clients par téléphone
    1.2 Répondre aux clients au comptoir
    1.3 Saisir la commande dans le système
    1.4 Faire payer le client (S'il est sur place)
    1.5 Imprimer la commande
  2. Construire la pizza
    2.1 Préparer la pâte
    2.2 Ajouter les ingrédients
    2.3 Faire cuire la pizza
    2.4 Emboîter la pizza
  3. Livrer la pizza
    3.1 Donner la pizza au client
    3.2 Charger la pizza dans la voiture
    3.3 Déplacer la voiture jusqu'au domicile du client
    3.4 Faire payer le client
~~~

Ici, on se préoccupe de la hiérarchie des appels, l'ordre d'exécution est plus ou moins important. Certaines étapes pourraient être répétées ou être ignorées selon certaines conditions. L'objectif est de déterminer les fonctions et sous-fonctions qui seront nécessaires pour résoudre votre problème. Notez bien qu'il y a toujours plusieurs solutions possibles pour résoudre un problème, donc il se peut que votre réponse diverge de la solution proposée.

Une fois le diagramme ci-dessus complété, on pourra déterminer par la suite comment on peut organiser ces fonctions sous forme de classe. On peut faire des classes avec des fonctions `static` qui contiendront chacune des fonctions ci-dessus. On peut aussi placer certaines sous-fonctions dans les classes de données. Par exemple, on pourrait juger pertinent de placer `1.5 imprimer commande` dans la classe de donnée `Commande` qui sera décrite ci-dessous.

# Analyse des données

La deuxième partie consiste à modéliser les données qui seront manipulées pour atteindre votre objectif. Pour l'instant, voyez les données comme des classes avec seulement des attributs, "getters", "setters" et constructeurs. On pourra ajouter d'autres fonctions par la suite si on le juge pertinent.

Voici un modèle de classe pour les données du processus de commande de pizza. Encore une fois, plusieurs solutions valides sont possibles. Toutes les variables d'une classe ont été ajoutées au diagramme pour que ce soit plus simple à comprendre. Les agrégations sont représentées sous forme de `List` de références, alors que les associations ne sont que de simples références.

![Diagramme Pizzaria](diagram/pizzeria.png)

# Étude de cas: Papa's Cupcakeria

![Titre](img/00_title.jpg)

Maintenant, c'est à votre tour. J'ai fait cet exercice à partir du jeu "Papa's Cupcakeria" dont j'ai mis les saisies d'écran pour bien illustrer le problème. Vous devez construire la hiérarchie des étapes de votre code ainsi que le modèle de classe pour les données. Imaginez que vous faites un programme qui fera les étapes nécessaires de construction de gâteau pour vous. Regardez les données disponibles dans les saisies d'écran et faites en le modèle de classe.

Le but du jeu consiste à faire faire par le joueur différentes tâches pour fabriquer des gâteaux selon les exigences des clients. Le jeu est composé de différentes stations (écrans) dans lesquelles on peut accomplir des tâches. On peut jongler entre ces stations si on veut construire les gâteaux plus rapidement. Par exemple, on peut garnir un gâteau pendant qu'un autre gâteau cuit.

Je vous explique donc les différentes étapes de construction de gâteaux, et vous faites les modèles en questions. Une solution sera offerte à la fin de la page. Inspirez-vous de l'exemple précédant de construction de pizza.

## Prise de commandes

Il y a 2 façons de recevoir des commandes, soit que le client vient au comptoir ou soit le serveur va prendre la commande à la table. Le client décrit les divers ingrédients qu'il désire pour son gâteau. Les gâteaux sont faits en paire, seule les garnitures sont unique à chaque gâteau. Les commandes sont numérotées.

![Prise de commandes](img/01_order.jpg)

## Remplissage

Le remplissage des moules se fait en 3 étapes. Il faut d'abord choisir le papier à mettre dans le fonds, ensuite choisir la saveur de la pâte et finalement remplir le mieux possible le moule. Les deux gâteaux auront le même papier et la même saveur. La quantité de pâte dans chaque moule est variable, il pourrait en avoir trop ou pas assez.

![Remplissage des moules](img/02_batter.jpg)

## Cuisson

On place ensuite les moules pleins dans le four. Lorsque le temps passe, les gâteaux augmentent en cuisson. Un gâteau peut-être trop ou pas assez cuit. Il y a une minuterie sur le four qui sonne lorsque le gâteau est prêt. Si les gâteaux sont trop cuit, on peut les jeter à la poubelle.

![Cuisson](img/03_bake.jpg)

## Garniture

Cette station est la plus complexe. On doit d'abord choisir la saveur du glaçage et essayer de l'appliquer uniformément sur le gâteau. Ensuite, on peut appliquer un coulis de diverse saveur (ex: chocolat, fraise), saupoudrer des confettis (ex: noix de coco, arc-en-ciel) et ajouter une décoration sur le dessus (ex: cerise, guimauve).

![Garnitures](img/04_build.jpg)

## Remise au client

La dernière étape consiste à remettre les gâteaux au client. Celui-ci évaluera la performance de chacune des stations, et les personnages donneront un pourboire. Chaque client a un nom et un nombre d'étoiles. Si la performance du joueur est bonne, le nombre d'étoiles pour ce personnage va augmenter au fil des parties.

![Remise au client](img/05_client.jpg)


## Étude de cas: solution

Solutions à venir

<details>
<summary>Solution: Hiérarchie de fonctions</summary>

~~~
main()
  1. Prendre la commande
    1.1 Écouter le client donner sa commande
    1.2 Prendre la commande du serveur
  2. Remplir les gâteaux
    2.1 Choisir papier de fonds
    2.2 Choisir la saveur de la pâte
    2.3 Remplir le moule
  3. Cuire les gâteaux
    3.1 Ajouter les gâteaux dans le four
    3.2 Retirer les gâteaux du four
    3.3 Jeter les gâteaux à la poubelle
  4. Garnir les gâteaux
    4.1 Choisir la saveur du glaçage
    4.2 Ajouter le glaçage
    4.3 Ajouter le coulis
    4.4 Ajouter les confétits
    4.5 Ajouter les décorations
  5. Remettre les gâteaux
    5.1 Présenter les gâteau au client
    5.2 Déterminer l'évaluation du client
    5.3 Récolter le pourboire
    5.4 Mettre à jour l'admiration du client
~~~

</details>

<details>
<summary>Solution: Diagramme de classe</summary>

![Diagramme de classe](diagram/cupcakeria.png)

</details>



