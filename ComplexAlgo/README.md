# INF2120 Laboratoires
Par Eric Pietrocupo

SOURCE: Rosen, Kenneth H.- Discrete Mathematics and Its Applications.-seventh edition.-McGrawHill,2012.-ISBN 9780073383095.

Note: Il pourrait y avoir certaines complications pour afficher les formules dans Gitlab. J'utilise `^`comme operateur d'exposant. Donc:

`x^2` = $x^2$

## Rappels théorique: Complexité Algorithmique

La complexité algorithmique s'intéresse à la quantité d'instructions exécutées ou de mémoire consommée indépendamment de la performance du matériel utilisé. La notation grand O cherche à définir la complexité d'un algorithme lorsque:

* Le pire cas possible survient.
* Lorsque le nombre d'éléments N tend vers l'infini.

C'est pourquoi on fait tomber les constantes dans les formules puisque N sera toujours plus grand qu'une constante C lorsque N tend vers l'infini. Normalement, on choisi le terme qui a le plus de poids dans une formule et on ignore le reste. On utilise un tableau pour comparer les formules entre elles.

| Complexité              | Nom               |
| -----------------       | ----------------- |
| $O(1)$                | Constante         |
| $O(log(n))$           | Logarithmique     |
| $O(n)$                | Linéaire          |
| $O(n.log(n))$         | Linéarithmique    |
| $O(n^b)$              | Polynomiale       |
| $O(b^n)$ si b > 1        | Exponentielle     |
| $O(n!)$               | Factorielle       |

Une formule fait partie d'une catégorie tant qu'elle ne dépasse pas la prochaine catégorie. Donc par exemple, $10000n^2$ et $n^8$ font partie de $O(n^b)$ puisqu'il n'atteignent jamais la complexité de $O(b^n)$. Voici une représentation graphique de cette table sur une échelle exponentielle.


![Complexité sous forme graphique](ComplexGraphique.png)

NOTE: Parfois on peut considérer $O(n^2), O(n^3), O(n^4)$ ... comme des sous classes séparés. Donc on pourrait dire qu'une fonction est $O(n^7)$ aulieu de $O(n^b)$.


## Exercice 1

Déterminez si ces fonctions font partie de $O(n)$

### a) $f(n) = 10$
### b) $f(n) = 3n+7$
### c) $f(n) = n^2 + n + 1$
### d) $f(n) = 5log(n)$
### e) $f(n) = 12n + 5n + 8$
### f) $f(n) = n(n+1)$
### g) $f(n) = 10log(n) + 45n + 22$

<details>
<summary>Solution</summary>

~~~
a) faux
b) vrai
c) faux
d) faux
e) vrai
f) faux
g) vrai
~~~

</details>

## Exercice 2
Déterminez la complexité de ces fonctions avec la notation grand O:

### a) $f(n) = 5n + 8n.log(n)$
### b) $f(n) = 2n^3 + n^2.log(n)$
### c) $f(n) = 5log(n) + 20$
### d) $f(n) = 450n + 3n.log(n) + 42$
### e) $f(n) = n^2 log(n) + 2 log(n)$
### f) $f(n) = n(n) + n + n! + n(n+n)$
### g) $f(n) = 48log(n) + 252n^2 + 3500n + 450 + 702n^7 + 2^n + 1234n.log(n)$

<details>
<summary>Solution</summary>


* a) $O(n.log(n))$
* b) $O(n^b)$ ou $O(n^3)$
* c) $O(log(n))$
* d) $O(n.log(n))$
* e) $O(n^b)$ ou $O(n^2)$
* f) $O(n!)$
* g) $O(2^n)$ ou $O(b^n)$


</details>


