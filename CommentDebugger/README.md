# Comment débugger


Cette section ne contient aucun exercice. Ce sont plutôt quelques techniques pour aider à trouver vos bugs puisque malheureusement, la grande partie du travail d'un programmeur est de débugger le code. J'ai un proverbe personnel en informatique qui dit:

`En théorie, ça fonctionne toujours, ... en pratique, ça ne fonctionne jamais`

Si on programme quelque chose qui fonctionne du premier coup, il y a possiblement une erreur de caché quelque part. En programmation, Il y a 3 types d'erreurs:

1. __Erreur de Compilation__: Ce sont des erreur d'utilisation du langage que le compilateur peut détecter. La plus grande difficulté de ces erreurs est de comprendre les messages d'erreur et de résoudre le problème.
2. __Bugs__: Les bugs sont des mauvais comportements du programme. Le programme compile et s'exécute, mais fait la mauvaise chose. Ce sont ces erreurs qui sont plus difficiles à trouver puisque l'on ne connaît pas toujours leur existence.
3. __Crash__: Ce sont des bugs qui terminent l'exécution du programme avec une exception ou un "Segmentation fault" (selon le langage utilisé). On connaît donc leur existence, ce qui est difficile est de les corriger puisque un "Seg. fault" laisse peu d'information sur l'endroit où le bug est survenu.

Par exemple, le code suivant essaie de parcourir un tableau, il contient une erreur de compilation, un bug et il échoue avec une exception:

~~~java
for (int i = 0; i <= tab.length;i++) { sum += sum + tab[i] }
~~~

## Techniques de traçage

### Trace avec message d'affichage

Un des objectifs premiers en débuggage est de pouvoir suivre le chemin que le programme a emprunté. Celui-ci est invisible, sauf si le programme affiche quelque chose. On peut donc le forcer à afficher quelque chose à divers endroits dans le programme.

Dans cet exemple, on vérifie si un programme entre dans une fonction ou dans une condition:

~~~java
public void fonction ( int nombre)
{   System.out.println("Le programme est entrée dans la fonction");
    if ( nombre > 28 )
    {   System.out.println("Le programme est entrée dans la condition");
    }
    nombre = nombre * 1200;
    System.out.println("Le programme a fait le calcul");
}
~~~

On peut donc tracer l'exécution du programme en affichant des messages sur la console à des endroits clé dans le code. On pourrait vérifier avec le code ci-dessus, si une fonction a été appelée ou si une condition a été validée.

### Affichage de variables

Parfois, on veut connaître l'état des variables en mémoire à un certain moment dans le programme. Donc on peut afficher des messages qui affichent le contenu des variables. En java, la définition de la fonction `toString()` sur les divers objets permet de rendre ces objets affichables pour le débuggage.

~~~java
public class Var
{   int valeur;
    Var ( int v ) { valeur = v; }
    public String toString() { return String.format("%d", valeur);}
} 

public void fonction ( int nombre )
{   Var v = new Var (nombre);
    System.out.println("La variable var contient "+ v);
    nombre = nombre * nombre;
    System.out.println("La nombre contient " + nombre);
}
~~~

Comme pour les traces, on place des messages aux endroits clé pour afficher l'état des variables.

### Utilisation du backtrace

Lors du lancement d'une exception, Java affiche la pile d'exécution qui contient la liste des appels de fonction au moment que le programme a planté avec le numéro de la ligne dans le code source. Voici un exemple de backtrace suite à une division par zéro.


~~~
Exception in thread "main" java.lang.ArithmeticException: / by zero
	at Main.fonction_C(Main.java:43)
	at Main.fonction_B(Main.java:37)
	at Main.fonction_A(Main.java:32)
	at Main.main(Main.java:10)
/home/ericp/.cache/netbeans/8.1/executor-snippets/run.xml:53: Java returned: 1
BUILD FAILED (total time: 0 seconds)
~~~
 
Cette trace permet de reconstruire la séquence d'appel qui a permis de générer cette erreur. L'information est tiré directement de la pile d'exécution. Donc on peut voir qu'à partir du `main()`:

* à la ligne 10, on a appelé la `fonction_A()` 
* à la ligne 32 on a appelé la `fonction_B()` 
* à la ligne 37 on a appelé la `fonction_C()`
* à la ligne 43 on a reçu une ArithmeticException avec le message `/ by zero` qui indique une division par zéro.

Dans ce cas ci, on peut facilement identifier où est la ligne de code erronée et faire les corrections nécessaires.

Dans certains langages, comme C et C++, on n'a pas ce luxe, et il faut faire appel à des outils de débuggage pour y arriver. Sinon, tout ce que l'on obtiendra est un `Segmentation fault`, et la seule façon de débugger sans outils est de mettre des messages de trace entre chaque ligne de code pour trouver la ligne problématique dans une zone que l'on juge suspecte. 

## Technique de substitutions

### Isolement d'un problème

Si on fait trop de code d'un coup, il pourrait être difficile de déterminer à quel endroit se trouve le bug. Donc on peut isoler le code en mettant en commentaire des sections que l'on ne désire pas encore valider. On ne fait que vérifier le fonctionnement de petites parties de code à la fois.

~~~java
public void fonction ()
{	int nombre = 0;	
	/* //portion mis en commentaire à ignorer pour l'instant
	for (int i = 0 ; i < 10 ; i++)
	{	nombre = nombre * i;
		i++;
	}
	*/

    // code a tester
	for (int i = 0 ; i < 10 ; i++)
	{	nombre = nombre - i;
		i++;
	}
}
~~~

### Création de sous-fonctions

Pour mieux isoler le code à tester, on peut aussi découper le code en sous-fonctions, cela a deux objectif: A) Tester une fonction à la fois, B) Éviter des effets de bord entre deux bout de code. Trouver le bug ci-dessous:

~~~java
public int fonction ( int nombre)
{	int i = 0;
	while ( i < 10 )
	{	nombre = nombre * i;
		i++;
	}
	while ( i < 10)
	{	nombre = nombre - i;
		i++;
	}	
	return nombre;
}
~~~

Si chaque `while` était dans sa propre fonction, la variable `i` de la première boucle n'aurait pas eu d'effet de bord sur la deuxième boucle.

### Fixer certaines valeurs

Dans un de mes jeux vidéo, j'avais implémenté la possibilité qu'un personnage soit empoisonné. Ceci demandait une séquence d'action qui impliquait beaucoup de hasard et était très difficile à tester. La séquence était:

1. Obtenir un monstre qui a la possibilité d'empoisonner le joueur.
2. Attendre que le monstre fassent sont attaque qui empoisonne.
3. Espérer le que personnage échoue son roulé de résistance.

Cela pourrait prendre beaucoup de tests avant de valider qu'un personnage peut bien se faire empoisonner. On peut donc fixer certaines valeurs comme faire que tous les monstres ont uniquement des attaques qui empoisonnent. On on est sûr que les 2 premières étapes seront toujours valide, ce qui laisse la 3e à tester.


## Outils de débuggage

### IDE: Integrated Development Environment

Beaucoup d'environnements de développement ont des outils de débuggage qui permet de faire la trace du programme et des variables. Cependant, notez bien que pour certaines applications, ce ne sera pas toujours possible d'utiliser se genre d'outils. Par exemple, j'ai eu à programmer un formulaire Adobe, la seule option était une console où l'on pouvait afficher des messages d'erreurs. En programmation de jeu vidéo, si vous affichez des messages sur la console, vous obtiendrez 60 messages par seconde, ce qui est difficile à tracer.

### gdb

GNU Debugger est surtout utilisé en C et C++. La syntaxe d'utilisation est très ardue, mais il offre toutes les fonctionnalités de trace et suivi de variables ainsi que l'affichage du `backtrace`. Il est important de compiler son programme avec l'option `-g` pour que les noms de variable et fonctions soient affichés au lieu de simples adresses mémoire.

### valgrind

Très utilisé pour de la programmation C et C++. Il offre toutes sortes de diagnostics, dont la vérification de "memory leak".

### Analyseurs de code

Ils existe différents outils d'analyse de code que j'ai plus ou moins utilisé. `Sonarqube` permet de vérifier la qualité du code de différents langages selon les bonnes pratiques. `Sourcetrail`permet de visualiser les séquences d'appels de fonctions. Il y a des `sclicers` de programme qui permet de chercher le code qui modifie une variable demandée permettant de corriger les effets de bord.

### Warnings

La majorité des compilateurs offrent des warnings, des lignes de codes qui sont jugés suspecte, mais qui ne crée pas d'erreur de compilation. Comme par exemple, utiliser une variable non-initialisé, variables non-utilisé, conversion de types suspecte, etc. Il faut prendre ces warnings un peu plus au sérieux et essayer de corriger la situation.

## Endroits à surveiller

Certaines portions de code sont plus propices à la présence de bugs. Voici certains endroits à porter attention.

### Valeurs limites

Certains bugs se trouvent proches des bornes, soit on inclue ou exclu des valeurs. Comme par exemple dans le code suivant

~~~java
int i = 1;
while (i <= 10) sum = sum + tab[i];
~~~

Ici, est-ce qu'on devrait inclure ou exclure 10? Devrait-on plutôt commencer à 0 puisque c'est un tableau? Si on veut parcourir un tableau en entier, java offre des foreach:

~~~java
for (int v: tab) sum = sum + v;
~~~

Beaucoup plus sécuritaire.

### Conditions

Les conditions peuvent être problématiques surtout si on en combine plusieurs. Dans cette situation, la logique booléenne n'est pas nécessairement évidente. Comme par exemple:

~~~java
if ( (a && !b) && ( c || d && e ) && ( b || !c ) )
~~~

En cas de doute, générez la table de vérité en essayant toutes les combinaisons possibles. Ici, il y a `2^5=32` possibilités. Sinon, on peut utiliser des parenthèses pour s'assurer que les termes seront résolus dans le bon ordre. On peut aussi découper les conditions en plusieurs conditions si les termes sont séparés par des `AND`. Voici un exemple de conversion de l'expression ci-dessus en utilisant des `if` imbriqués:

~~~java
if (a && !b)
{	if ( c || (d && e))
	{   if ( b || !c )
		{
		}
	}
}
~~~

Il y aurait peut-être une façon de simplifier l'expression booléenne sans sacrifier le comportement. Utilisez les propriétés des expressions booléennes: Associativité, distributivité, de Morgan, etc.


### Absence de blocs

Dans les langages qui utilisent des blocs encadrés par des accolades, le manque d'accolades peut créer certains bugs surtout lors de la modification de code. Trouvez l'erreur ci-dessous:

~~~java
int i = 0;
while ( i < 10 )
   sum = sum + tab[i];
   i++;
~~~

Ici, on obtient une boucle infinie. Il est préférable de toujours ajouter les accolades. Si votre bloc ne contient qu'une seule ligne de code, il est préférable de le mettre sur une seule ligne. Si on veut ajouter une 2e ligne de code plus tard, on ajoutera les accolades à ce moment. Dans l'exemple ci-dessous, si on veut ajouter une 2e ligne de code, c'est plus facile de voir que des accolades sont requises.

~~~java
for (int v: tab) sum = sum + v;
   # ligne de code à ajouter
~~~

Tandis que dans le code ci-dessous, on serait tenté d'ajouter une ligne de code directement en dessous:

~~~java
for (int v: tab)
	sum = sum + v;
	# ligne de code à ajouter
~~~

Notez que dans certains langages comme `Python`, il n'y a pas de blocs, on utilise l'indentation pour déterminer le bloc d'appartenance.

### Break manquant

Une erreur classique des `switch-case`, l'oubli de l'instruction `break` lorsque l'on ne veut pas faire plusieurs conditions.

~~~java
switch(variable) {
  case X: System.out.println ("Variable X");
  break;
  case Y: System.out.println ("Variable Y");
  default: System.out.println ("Rien");
}
~~~

Ici, `Y` lancera aussi la condition de `default` puisqu'il manque un `break`.

## Protection

On peut essayer de prévenir les bugs avec de la programmation défensive. Cela augmente la quantité de code, mais aussi la sécurité et solidité de celui-ci. L'objectif est de s'assurer que les situations problématiques ne se présentent jamais.

### Utilisation de gardes

On peut par exemple s'assurer que les paramètres d'une fonction respectent le contrat d'utilisation pour s'assurer qu'il n'y aura pas d'erreurs dans le traitement. Comme par exemple:

~~~java
public void fonction ( Object param1, char param2 )
{	if ( param1 != null)
	{	if ( param2 >= 'a' && param2 <= 'z' )
		{	 //faire le traitement ici
		}
		else System.out.println("Le parametre 2 doit etre entre a et z");
	}
	else System.out.println("Le parametre 1 ne peut etre null");
}
~~~

La structure en `if` imbriquée peut devenir difficile à lire s'il y a beaucoup de conditions ou si le code de traitement est très long. De plus, le message d'erreur est loin de la condition. Donc une autre façon d'écrire ces conditions est la suivante:

~~~java
public void fonction ( Object param1, char param2 )
{	if ( param1 == null)
	{	System.out.println("Le parametre 1 ne peut etre null");
		return;
	}
	if ( param2 < 'a' && param2 > 'z' )
	{   System.out.println("Le parametre 2 doit etre entre a et z");
		return;
	}

	//faire le traitement ici
}
~~~

On peut terminer le programme immédiatement si on le juge pertinent. Voici comment on pourrait implémenter ce genre de garde:

~~~java
if ( param1 == null)
{	System.out.println("Le paramètre 1 ne peut être null, C'EST COMPLÈTEMENT INACCEPTABLE");
	System.exit(1);
}
~~~

Le paramètre de `exit()` est un code d'erreur. On pourrait numéroter les erreurs possibles pour que l'utilisateur puisse identifier facilement la source du problème.

### Assertions 

Les assertions sont comme des gardes, mais sont intégrés dans les librairies du langage. Elle fait partie de la programmation par contrat. Un des avantages des assertions est qu'elles peuvent être désactivées lors du déploiement du logiciel ce qui évite de perdre de la performance en faisant des vérifications excessives.

Il faut d'abord activer les assertions à la ligne de commande généralement durant la phase de développement. On peut le faire en Java avec les options `-ea` ou `-enableassertions`. La désactivation se fait avec `-da` ou `-disableassertions`. Voici l'exemple ci-dessus converti avec des assertions:

~~~java
public void fonction ( Object param1, char param2 )
{	assert param1 != null : " Le paramètre 1 ne peut être null";
	assert ( param2 >= 'a' && param2 <= 'z' ) : " Le paramètre 2 doit être entre a et z";

	//faire le traitement ici
}
~~~

Si l'assertion échoue, le programme lancera une exception de type `AssertionError` avec la chaîne de caractère mis après le `:` comme message d'erreur.

SOURCE: https://www.geeksforgeeks.org/assertions-in-java/

### Vérification pour `null`

Identifié comme étant l'erreur qui vaut un trillion de dollars, ce sont les fameux `NullPointerException`. L'origine du `null` vient du langage `C`. Il n'était pas problématique dans ce langage puisqu'il n'était pas orienté objet. Donc on ne pouvait appeler une fonction sur un "Null Pointer" comme c'est le cas en java:

~~~java
public void fonction ( Object param1 )
{	System.out.println(param1.hashCode());
}
~~~

Donc il est important d'ajouter des gardes pour vérifier la présence de null:

~~~java
public void fonction ( Object param1 )
{	if (param1 != null) System.out.println(param1.hashCode());
}
~~~

Le langage Kotlin a même créé un opérateur spécial nommé l'opérateur Elvis `?:` pour intégrer la vérification de `null` lors de l'appel de fonctions. Pour plus d'information, voir:

https://kotlinlang.org/docs/null-safety.html

## Tests automatisés

Pour se simplifier la vie, on peut faire tester notre programme par notre ordinateur. Cela demande un peu plus de travail, mais c'est très valorisant quand notre programme passe tous les tests. Il y a divers types de tests pour différentes utilisations. Ici, on va n'en voir que quelques-uns dont certains seront vu dans d'autres cours.

### Test unitaires
Les tests unitaires sont des tests très simples qui vérifient le comportement d'une fonction. Une fonction est constituée de paramètres d'entrées et de sortie. Donc si on appelle une fonction plusieurs fois avec les mêmes paramètres d'entrées, la sortie sera toujours pareil. On utilise ce principe pour créer des tests qui vérifieront notre fonction. Comme par exemple, voici une fonction:

~~~java
public class Calculator
{	public static int add ( int a, int b)
	{	return a + b;
	}
}
~~~

On peut s'attendre que par exemple si on appèle `add(2, 3)` la réponse sera toujours 5. On peut donc utiliser des assertions pour valider le comportement de notre fonction. En Java on utilise `Junit`, en C/C++ il y a `Cunit`, en D ils sont intégrés au langage. Voici un exemple de tests pour la fonction ci-dessus:

~~~java
class CalculatorTest 
{	@Test                                               
    @DisplayName("Vérification de l'addition")   
    void testAddition() {
        assertEquals(5, Calculator.add(2, 3), "ÉCHEC: Addition de deux positif");  
        assertEquals(-3, Calculator.add(5, -8), "ÉCHEC: Addition d'un négatif");
        assertEquals(-17, Calculator.add(-4, -13), "ÉCHEC: Addition de deux négatif");    
    }
}
~~~

Ici, notre fonction de tests, lance plusieurs assertions dans ce but de valider différents cas de figure qui peuvent survenir lors d'une addition. On compare la réponse avec le résultat de la fonction, et on donne un message d'erreur à afficher si les deux valeurs ne sont pas égales. Junit identifiera ces fonctions de test avec l'annotation `@Test`. Il y a beaucoup plus de choses que l'on peut faire avec Junit. Voir la référence ci-dessous.

SOURCE: https://www.vogella.com/tutorials/JUnit/article.html

### Fonctions de débuggages

Lorsque vous développez des nouvelles fonctionnalités, vous devez écrire du code de tests pour vérifier leur fonctionnement. Une fois terminé, on a tendance à jeter ce code de tests à la poubelle. Cependant, on a intérêt à conserver ce code pour soit faire des tests automatisés plus tard, ou soit re-tester les fonctionnalités en questions si des changements sont apportés. 

Personnellement, je crée un module, ou une classe qui contient tous ces bouts de code de tests et je les appelle du main au besoin. Par exemple:

~~~java
public class TestManuels
{	public static void test_fonctionalite_a ()
	{	// code de tests
	}
	public static void test_fonctionalite_b ()
	{	// code de tests
	}
}

public class Main 
{	public static void main(String[] args) 
	{	//TestManuels.test_fonctionalite_a();
		TestManuels.test_fonctionalite_b();
		//programme_principal();
	}

	public static void programme_principal() 
	{	// code du programme principal
	}
}
~~~

Dans le `main()`, on place toutes les fonctions de tests et celle du programme principal. On dé-commente ce que l'on veut exécuter. On peut donc rapidement changer le comportement de notre programme selon le besoin.


### Utilitaire Bats

Cet utilitaire est beaucoup plus pratique pour des logiciels qui s'exécutent à la ligne de commande avec divers paramètres. L'idée est que l'on donne une commande et on indique le résultat attendu. On fait plusieurs tests de cette façon que l'on peut faire lancer la série pour vérifier si notre programme fonctionne toujours adéquatement. Voici un exemple de tests unitaire d'un script bats pour un programme qui fait une addition:

~~~
@test "FXLC: add " {
	run ./fxlc add 2 3
	[ "$status" -eq 0 ]
	[ "$output" = "2 + 3 = 5" ]
}
~~~

Le code de Bats est disponible ici:

https://github.com/bats-core/bats-core.git

## Techniques ésotériques

Ces techniques semblent possiblement farfelues, mais par expérience, je peux vous garantir qu'elles fonctionnent.

### Déléguer à son inconscient 
Lorsque vous avez un problème, ne vous entêtez pas à trouver une solution, vous vous épuiserez pour rien. Votre subconscient est probablement déjà en train de chercher une solution. Donc après un certain temps, aller programmer autre chose, allez faire d'autres activités, prenez une pause, etc. Il est très probable que vous allez trouver des pistes de solution au fil du temps. Donc laissez du temps à votre subconscient pour qu'il puisse faire son travail.

### Rubber ducking
La technique originale consiste à mettre un canard en plastique sur son bureau (d'où le nom de la technique) et de lui raconter verbalement son problème. On explique le bug, avec les tests fait jusqu'à maintenant. Au fil des explications, il est possible que vous obteniez la solution par vous même. Encore une fois, c'est votre subconscient qui fait le travail.

Cette technique peut se faire avec d'autres entités que des canards en plastique. Des animaux en peluche font aussi bien l'affaire. Si vous êtes intimidé à l'idée de parler à des objets inanimés, on peut aussi le faire avec des vrai personne qui font semblant de nous écouter ou simplement un animal.

Cela peut aussi se faire par écrit. Imaginez que vous posez une question sur un forum de discussion. Tapez un message dans notepad qui explique votre problème avec les étapes de débuggage fait jusqu'à maintenant et vous trouverez possiblement la solution avant de finir l'écriture de votre message.

## Changer sa méthode de travail

On peut réduire l'occurrence de problèmes en modifiant notre façon de travailler. Voici quelques méthodes que j'utilise personnellement.

### Bite and chew

L'idée est de prendre des petites bouchés à la fois. On implémente une petite fonctionnalité, on test, on répète. Si vous implémentez trop de truc d'un seul coup, vous aurez de la difficulté à débugger le code par la suite. Donc vaut mieux découper le problème en petit problème et tester individuellement chacun de ceux-ci.

### Période de programmation courte
Par expérience, il est préférable de programmer plusieurs courtes séances que quelques longues sérance. Un séance devrait durer entre 2 et 3 heures, avec des pauses de 10 minutes aux 50 minutes, avec au plus 2 séances par jours si vous êtes en retard. Essayer d'espacer un bon 8-12 heures entre ces deux séances.

Ceci permet de prendre l'avantage du fait que :
* Au début, il faut comprendre ce qu'il reste à faire et se mettre "dedans", donc cela prends environ 15-30 minutes pour y arriver.
* Plus on programme, plus on s'épuise, et moins on devient habile à performer. On mettra plus d'effort pour arriver à notre objectif plus que le temps avance.
* On peut tirer avantage de notre subconscient. Lorsque l'on fait autre chose, note subconscient continu à travailler pour trouver des solutions. Donc vous trouverez des meilleures solutions à vos problèmes de cette façon.


### Journal de développement
Il est important de conserver l'état d'esprit dans lequel vous étiez à la dernière séance de programmation. Ceci réduira le temps nécessaire pour se mettre "dedans". Écrivez un journal et indiquez les points suivants:

* Objectif à atteindre pour cette séance de programmation.
* Ce que vous avez réussi à faire.
* Les choses à faire aux prochaines séances.
* Les fonctionnalités plus complexe à reporter à plus tard.
* Le temps écoulé pour faire ces tâches.

Vous pouvez mettre ces informations dans un fichier texte. Si vous utilisez Git, je recommande forcément de mettre l'information dans vos commit git. Je recommande aussi de commiter au moins 1 fois par jour si vous touchez à votre code, surtout si vous travaillez en équipe. Les autres programmeurs pourront voir ce que vous avez fait.

### Quoi faire en conditions extrêmes

Vous pourrez avoir à travailler dans des conditions extrêmes. Comme par exemple:

* Chaleur
* Fatigue
* Maux de tête
* Bruits
* Distractions

C'est certain que c'est plus difficile de travailler dans ces conditions. Donc il faut voir si on pourrait faire des choses plus légères qui ne demande pas beaucoup de concentration dans le but de faire avancer le projet. Voici quelques exemples:

* Écrire de la documentation
* Nettoyer et déplacer du code
* Écrire des tests unitaire
* Faire des tests manuel de débuggage

L'idée est de mettre en ordre le projet dans le but de rendre la prochaine vraie séance de programmation plus productive.

### Programmation en paire (Pair Programming)

C'est une technique utilisée pour des bouts de code critique. Deux programmeurs travaillent en même temps devant le même écran sur le même bout de code. L'idée est qu'à deux programmeurs, on a moins de chance de faire d'erreurs ou d'oublier des choses. Il y a beaucoup de documentation sur le sujet, aller y jeter un coup d'oeil. (À proscrire pour les travaux pratiques individuels)

## Conclusion

Cela fait pas mal le tour du sujet, il y a plusieurs façons de débugger. Essayez diverses méthodes et utilisez ce qui vous convient. Vous développerez vos propres techniques de débuggages avec le temps. Pour en apprendre davantage, voici d'autres sources sur le sujet.

SOURCES COMPLÉMENTAIRES:

https://www.freecodecamp.org/news/what-is-debugging-how-to-debug-code/

https://www.codecademy.com/resources/blog/how-to-debug-your-code/

https://www.codementor.io/@mattgoldspink/how-to-debug-code-efficiently-and-effectively-du107u9jh

https://users.cs.utah.edu/~germain/PPS/Topics/debugging_programs.html



