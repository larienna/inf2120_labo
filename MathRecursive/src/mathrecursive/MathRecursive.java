/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mathrecursive;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Programme qui genere des suites de nombres et font des operation mathematiques recursivement.
 * @author Eric Pietrocupo <ericp@lariennalibrary.com>
 */
public class MathRecursive
{

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args)
   {
      System.out.println("___Suites de nombres___");
      System.out.println("8e nombre triangulaire = " + nombreTriangulaire(8) );
      System.out.println("5e nombre factoriel = " + nombreFactoriel(5) );
      System.out.println("10e nombre de fibonacci = " + nombreFibonacci(10) );
      
      System.out.println("___Operations Mathematiques___");
      ArrayList<Integer> liste = new ArrayList<>();
      liste.add(5);
      liste.add(13);
      liste.add(21);
      liste.add(43);
      System.out.println("Soit la liste suivante: " + liste );
      System.out.println("La somme donne = " + sommeInteger(liste) );
      
      System.out.println("___Permutations binaires de 4 bits___");
      permutationsBinaires(4);      
   }
   
   public static int nombreTriangulaire (int n)
   {  if (n == 0) return 0;
      return n + nombreTriangulaire(n-1);
   }
   
   public static int nombreFactoriel (int n)
   {  if ( n == 0) return 1;
      return n * nombreFactoriel(n-1);      
   }
   
   public static int nombreFibonacci (int n)
   {  if ( n == 0) return 0;
      if ( n == 1) return 1;
      return nombreFibonacci(n-1) + nombreFibonacci(n-2);   
   }
   
   public static int sommeInteger ( ArrayList<Integer> liste)
   {  if ( liste.isEmpty() ) return 0;
      Integer nb = liste.remove(0);
      return nb + sommeInteger (liste);   
   }
   
   public static void permutationsBinaires (int nbits)
   {  char []tab = new char[nbits];
      permutationsBinairesRecursif(tab, nbits);
   }
   
   private static void permutationsBinairesRecursif ( char [] tab, int nbits)
   {  if ( nbits == 0) 
      {  System.out.println(Arrays.toString(tab));      
         return;
      }      
      int index = tab.length - nbits;
      tab[index] = '0';
      permutationsBinairesRecursif(tab, nbits - 1);
      tab[index] = '1';
      permutationsBinairesRecursif(tab, nbits - 1);
   
   }
   
}
