# INF2120 Laboratoires
par Eric Pietrocupo

## Exercice 1: Suites de nombres récursives

Faites une trace récursive pour les 5 premiers nombres de diverses suites de nombres. La suite triangulaire est donnée à titre d'exemple:

### Exemple: Nombres Triangulaires
Cette suite additionne le n-ième nombre au nombre précédant de la suite. 

Définition: `Ni = i + Ni-1` où `N0 = 0`

ex: 1,3,6,10,15

On peut tracer récursivement de cette façon:

~~~
N0 = 0
N1 = 1 + N0 = 1 + 0 = 1
N2 = 2 + N1 = 2 + 1 = 3
N3 = 3 + N2 = 3 + 3 = 6
N4 = 4 + N3 = 4 + 6 = 10
N5 = 5 + N4 = 5 + 10 = 15
~~~

On peut aussi représenter visuellement cette suite de nombres avec des triangles d'élements:

![Suite Triangulaire](triangular.png)

### 1a: Nombres Factoriel

Cette suite de nombres fait la multiplication avec le n-ième nombre. 

Definition: `Ni = i x Ni-1` où `N0 = 1`

ex: 1,2,6,24,120

Faites la trace récursive:

<details>
<summary>Solution</summary>

~~~
N0 = 1
N1 = 1 x N0 = 1 x 1 = 1
N2 = 2 x N1 = 2 x 1 = 2
N3 = 3 x N2 = 3 x 2 = 6
N4 = 4 x N3 = 4 x 6 = 24
N5 = 5 x N4 = 5 x 24 = 120
~~~

</details>
 
### 1b: Nombres de Fibonacci

Cette suite de nombres prend comme nouveau nombre la somme des deux nombres précédant.

Définition: `Ni = Ni-1 + Ni-2` où `N0 = 0` et `N1 = 1`

ex: 1,2,3,5,8

<details>
<summary>Solution</summary>

~~~
N0 = 0
N1 = 1 
N2 = N1 + N0 = 0 + 1 = 1
N3 = N2 + N1 = 1 + 1 = 2
N4 = N3 + N2 = 2 + 1 = 3
N5 = N4 + N3 = 3 + 2 = 5
N6 = N5 + N4 = 5 + 3 = 8
~~~

</details>

## Exercice 2: Programmation de math récursive

### 2a: Suites de nombres

On peut maintenant implémenter récursivement ces suites de nombres. Faites-vous un programme principal qui possède les fonctions statiques suivante pour générer des suites de nombres:

~~~java
public static int nombreTriangulaire (int n)
public static int nombreFactoriel (int n)
public static int nombreFibonacci (int n)
~~~

Vous ne pouvez pas ajouter de sous-fonctions. Les fonctions ci-dessus sont récursives, donc pas de boucles.

### 2b: Somme de nombres

Ensuite, faites une fonction récursive qui prend une liste d'entiers et en fait la somme. Voici la définition:

~~~java
public static int sommeInteger ( ArrayList<Integer> liste)
~~~

Vous ne pouvez déclarer de nouvelles sous-fonction et vous ne pouvez pas utiliser de boucles.

## Exercice 3: Permutations binaires

Faites une fonction récursive qui affiche toutes les combinaisons de bit possible qu'un nombre peut prendre. On passe en paramêtre le nombre de bits et on savegarde l'affichage dans un tableau de caractères. Voici la signature des fonctions, la première appèle la deuxième qui est récursive.

~~~java
public static void permutationsBinaires (int nbits)
private static void permutationsBinairesRecursif ( char [] tab, int nbits)
~~~

Les tableaux sont affichés successivement sur la console. Voici un exemple de début de sortie pour des permutations de 4 bits.

~~~
[0, 0, 0, 0]
[0, 0, 0, 1]
[0, 0, 1, 0]
[0, 0, 1, 1]
[0, 1, 0, 0]
...
~~~

## Main de tests

Voici un programme `main()` pour tester vos fonctions

~~~java
public static void main(String[] args)
   {
      System.out.println("___Suites de nombres___");
      System.out.println("8e nombre triangulaire = " + nombreTriangulaire(8) );
      System.out.println("5e nombre factoriel = " + nombreFactoriel(5) );
      System.out.println("10e nombre de fibonacci = " + nombreFibonacci(10) );
      
      System.out.println("___Operations Mathematiques___");
      ArrayList<Integer> liste = new ArrayList<>();
      liste.add(5);
      liste.add(13);
      liste.add(21);
      liste.add(43);
      System.out.println("Soit la liste suivante: " + liste );
      System.out.println("La somme donne = " + sommeInteger(liste) );
      
      System.out.println("___Permutations binaires de 4 bits___");
      permutationsBinaires(4);      
   }
~~~

La sortie est:

~~~
___Suites de nombres___
8e nombre triangulaire = 36
5e nombre factoriel = 120
10e nombre de fibonacci = 55
___Operations Mathematiques___
Soit la liste suivante: [5, 13, 21, 43]
La somme donne = 82
___Permutations binaires de 4 bits___
[0, 0, 0, 0]
[0, 0, 0, 1]
[0, 0, 1, 0]
[0, 0, 1, 1]
[0, 1, 0, 0]
[0, 1, 0, 1]
[0, 1, 1, 0]
[0, 1, 1, 1]
[1, 0, 0, 0]
[1, 0, 0, 1]
[1, 0, 1, 0]
[1, 0, 1, 1]
[1, 1, 0, 0]
[1, 1, 0, 1]
[1, 1, 1, 0]
[1, 1, 1, 1]
~~~

[Solutions](src/mathrecursive/MathRecursive.java)

