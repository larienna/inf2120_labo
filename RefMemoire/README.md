# INF2120 Laboratoires
par Eric Pietrocupo

## Exercice: Références et état de la mémoire

Cet exercice essai de simuler l'évolution de l'état de la mémoire dans le but de bien comprendre où les références et les objets alloués par `new` se retrouvent.

Il est recommandé de faire le dessin sur une feuille de papier et d'effacer lorsque les changements surviennent.

### Rappels théoriques

* __Pile(Stack)__: Mémoire qui accumule les variables sous forme de pile (FILO: First In Last Out). Donc les éléments sont retirés en ordre inverse d'ajout. Pour les besoins de l'exercice, les adresses mémoire de la pile commenceront à partir de 00 en montant.
* __Tas(Heap)__: Mémoire qui est utilisé allouée au besoin du programme par l'opérateur `new`. Pour les besoins de l'exercice, les allocations seront fait en ordre à partir de 99 en descendant. Le ramasse-miettes laissera des trous en mémoire.
* __Types primitifs__: Variables de type int, char, float, double, etc. Ces variables sont allouées sur la pile.
* __Références__: Variables qui contient l'adresse mémoire d'un objet. Ces variables sont allouées sur la pile sauf lorsqu'ils font partie d'un objet (ex: tableau de références).
* __Objets__: Objets fait à l'aide d'une classe et de l'opérateur `new`. Ceux-ci sont toujours placés dans le tas, et seront pointés par une référence.
* __Case mémoire__: Pour les besoins de l'exercice, une case mémoire sera numéroté de 00 à 99. Elle contient 1 élément: variable, objet, élément de tableau, etc.

Pour conserver cet exercice simple, on ne modifiera pas la pile lors d'un appel de fonction.

### Code

Il y a deux morceaux de code, premièrement la classe `Entier` qui ne fait que contenir un int avec des getters, setters et constructeurs.

~~~java
public class Entier {
   private int valeur;
   
   Entier ( int v){ valeur = v; }
   
   Entier (Entier ref){ valeur = ref.valeur; }
   
   int get (){ return valeur; }
   
   void set ( int v){ valeur = v; }
   
   @Override
   public String toString (){ return "" + valeur;}
}
~~~


Ensuite il y a le programme principal.

~~~java
public static void main(String[] args) {
      int a = 2;
      Entier ra = new Entier(3);
      //----- Etat de la memoire 01 -----
      Entier rb = new Entier(4);
      Entier rc = new Entier(5);
      rb = ra;
      //----- Etat de la memoire 02 -----
      ra = null;
      int c = a + rb.get(); 
      int b = 6;
      Entier rd = new Entier(rc);
      Entier tab[] = new Entier[4];
      //----- Etat de la memoire 03 -----
      tab[0] = rd;
      tab[1] = new Entier(rb.get());
      tab[2] = new Entier(b + a);
      tab[3] = new Entier(7);
      tab[3].set(6);
      rb.set(tab[2].get() * 2);
      //-----Etat de la memoire 04 -----
      System.out.println("int a     = " + a);
      System.out.println("int c     = " + c);
      System.out.println("int b      = " + b);
      System.out.println("Entier RA = " + ra);
      System.out.println("Entier RB = " + rb);
      System.out.println("Entier RC = " + rc);
      System.out.println("Entier RD  = " + rd);
      System.out.println("Entier tab = " + Arrays.toString(tab));
   }
~~~


### Question 01: État de la mémoire

Faite un diagrame de la mémoire et faites les mises à jour après chaque ligne de code exécuté. 

A chaque position mis en commentaire dans le code, validé votre schéma avec les solutions ci-dessous. Voici la solution pour la première position à titre d'exemple.

N.B.: J'utilise "nl" pour indiquer "null"

État de la memoire 01
~~~
00 [ 2] a          99 [ 3] 
01 [99] ra         98 [  ] <-Tas
02 [  ] <-Pile     97 [  ]
~~~



<details>
<summary>Solution: État de la memoire 02</summary>


~~~
00 [ 2] a          99 [ 3] Entier
01 [99] ra         98 [ 4] Entier(à effacer)
02 [99] rb         97 [ 5] Entier
03 [97] rc         96 [  ] <-Tas
04 [  ] <-Pile     95 [  ]
~~~

</details>

<details>
<summary>Solution: État de la memoire 03</summary>

~~~
00 [ 2] a          99 [ 3] Entier
01 [nl] ra         98 [ 4] Entier(à effacer)
02 [99] rb         97 [ 5] Entier
03 [97] rc         96 [ 5] Entier
04 [ 5] c          95 [nl] Entier tab[0]
05 [ 6] b          94 [nl] Entier tab[1]
06 [96] rd         93 [nl] Entier tab[2]
07 [95] tab        92 [nl] Entier tab[3]
08 [  ] <-Pile     91 [  ] <-Tas
~~~

</details>

<details>
<summary>Solution: État de la memoire 04</summary>

~~~
00 [ 2] a          99 [16] Entier
01 [nl] ra         98 [ 4] Entier(à effacer)
02 [99] rb         97 [ 5] Entier
03 [97] rc         96 [ 5] Entier
04 [ 5] c          95 [96] Entier tab[0]
05 [ 6] b          94 [91] Entier tab[1]
06 [96] rd         93 [90] Entier tab[2]
07 [95] tab        92 [89] Entier tab[3]
08 [  ] <-Pile     91 [ 3] Entier
                   90 [ 8] Entier
                   89 [ 6] Entier
                   88 [  ] <-Tas
~~~

</details>


### Question 02: Sortie à l'écran

Quel sera le résultat affiché à l'écran?

<details>
<summary>Solution: Résultat à l'écran</summary>

~~~
int a     = 2
int c     = 5
int b      = 6
Entier RA = null
Entier RB = 16
Entier RC = 5
Entier RD  = 5
Entier tab = [5, 3, 8, 6]
~~~

</details>
