/*
 * Copyright 2023 Eric Pietrocupo <ericp@lariennalibrary.com>.
 */

package refmemoire;

import java.util.Arrays;

/**
 * Petit programme d'exemple pour jongler avec les références 
 */
public class RefMemoire
{

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {
      int a = 2;
      Entier ra = new Entier(3);
      //----- Etat de la memoire 01 -----
      Entier rb = new Entier(4);
      Entier rc = new Entier(5);
      rb = ra;
      //----- Etat de la memoire 02 -----
      ra = null;
      int c = a + rb.get();  
      int b = 6;
      Entier rd = new Entier(rc);
      Entier tab[] = new Entier[4];
      //----- Etat de la memoire 03 -----
      tab[0] = rd;
      tab[1] = new Entier(rb.get());
      tab[2] = new Entier(b + a);
      tab[3] = new Entier(7);
      tab[3].set(6);
      rb.set(tab[2].get() * 2);        
      //-----Etat de la memoire 04 -----
      System.out.println("int a     = " + a);
      System.out.println("int c     = " + c);
      System.out.println("int b      = " + b);
      System.out.println("Entier RA = " + ra);        
      System.out.println("Entier RB = " + rb);        
      System.out.println("Entier RC = " + rc);        
      System.out.println("Entier RD  = " + rd);
      System.out.println("Entier tab = " + Arrays.toString(tab));             
   }
   
}
