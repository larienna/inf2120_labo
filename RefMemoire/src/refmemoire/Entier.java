/*
 * Copyright 2023 Eric Pietrocupo <ericp@lariennalibrary.com>.
 */

package refmemoire;

/**
 * Classe qui contient un entier. Classe maison qui n'aura pas les mêmes 
 * privilèges que Integer.
 */
public class Entier {
   private int valeur;
   
   Entier ( int v){ valeur = v; }
   
   Entier (Entier ref){ valeur = ref.valeur; }
   
   int get (){ return valeur; }
   
   void set ( int v){ valeur = v; }
   
   @Override
   public String toString (){ return "" + valeur;}

}
