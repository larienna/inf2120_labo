# INF2120 Laboratoires
## Exercices complémentaires
Par Eric Pietrocupo

Le dépot contient des exercices complémentaires. Les codes sources sont disponibles s'il y a lieu.

### Semaine 1
* [Questions sur les références](QstReference)
* [Encodage de couleurs RGB(difficile)](CouleurRGB)

### Semaine 2
* [Références: État de la mémoire](RefMemoire)
* [Usine à gâteaux](UsineGateaux)

### Semaine 3
* [Héritage et énumération](Heritage)

### Semaine 4
* [Introduction à l'analyse](IntroAnalyse)

### Semaine 5
* [Comment débugger](CommentDebugger)

### Semaine 8
* [Mathematique récursive](MathRecursive)

### Semaine 9
* [Piles génériques](PileGenerique)

### Semaine 10
* [Complexité algorithmique](ComplexAlgo)
* [Algorithmes de Tri](AlgoTrie)


### Optionel

Projets de jeux vidéo en libgdx fait à partir du livre "Learning Libgdx game Development":

* [Cyber Glider: projet personnel](https://gitlab.com/larienna/cyber-glider)
* [Canyon Bunny: projet du livre](https://github.com/PacktPublishing/Learning-LibGDX-Game-Development-Second-Edition)
