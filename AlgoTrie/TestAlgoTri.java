package testalgotri;

import java.util.Arrays;

/**
 * Petite classe pour tester l'algorithme de tri rapide avec le code du prof.
 */
public class TestAlgoTri
{

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args)
   {  Integer tab[] = {1,5,0,2,7,4,3};
      System.out.println("Debut: " + Arrays.toString(tab));
      TriRapide ( tab, 0, tab.length -1);
      System.out.println("Fin: " + Arrays.toString(tab));
      
   }
   
   public static < E extends Comparable< E>>
           void TriRapide ( E[]tab, int debut, int fin)
           {   
              if ( debut < fin)
              {   int j = partitionner(tab, debut, fin);
                 System.out.println(": " + Arrays.toString(tab));
                 TriRapide (tab, debut, j);
                 TriRapide (tab, j+1, fin);
              }
                 
           }
   
   public static < E extends Comparable< E>>
           int partitionner(E[] tab, int debut, int fin)
   {
      int pindex = (debut + fin) / 2;
      E pivot = tab[pindex];
      System.out.println("Index = "  + pindex);
      System.out.println("Pivot = "  + pivot);
      int i = debut - 1;
      int j = fin + 1;
      while (i < j)
      {
         do
         {
            --j;
         }
         while (tab[j].compareTo(pivot) > 0);
         do
         {
            ++i;
         }
         while (tab[i].compareTo(pivot) < 0);
         if (i < j)
         {
            E temp = tab[i];
            tab[i] = tab[j];
            tab[j] = temp;
         }
      }
      return j;
   }
   
}

