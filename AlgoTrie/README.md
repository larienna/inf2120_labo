# INF2120 Laboratoires
par Eric Pietrocupo

## Exercice: Algorithmes de tri

L'objectif de ces exercices est de vous familiariser avec les divers algorithmes de tri. On n'aura pas besoin de programmer pour cette partie. Je donne un tableau de nombre non-trié, il faut appliquer chacun des algorithmes et faire la trace du tableau à chaque itération.

Voici les séquences de nombres à expérimenter:

`A: [1,5,0,2,7,4,3]`

`B: [3,4,1,7,2,6,5,0]`

`C: [5,4,3,2,1]`

Je vais donner des versions animés de ces algorithmes disponibles sur wikipedia. Notez-bien qu'il y a plusieurs versions du tri-rapide, donc utilisez algorithme donné en classe. Entre autres, le choix du pivot peut changer d'un algorithme à un autre.

J'ai trouvé le site suivant qui permet aussi de visualiser diverses sortes de tri.

[Hacker Earth](https://www.hackerearth.com/practice/algorithms/sorting/bubble-sort/visualize/)

SOURCE: Les animations on étés prises de wikipedia.

## Tri Bulle

![Démonstration du tri bulle](https://upload.wikimedia.org/wikipedia/commons/c/c8/Bubble-sort-example-300px.gif)

`A: [1,5,0,2,7,4,3]`

<details>
<summary>Solution</summary>

~~~
[1,5,0,2,7,4,3]
[1,0,2,5,4,3,7]
[0,1,2,4,3,5,7]
[0,1,2,3,4,5,7]
~~~

</details>


`B: [3,4,1,7,2,6,5,0]`

<details>
<summary>Solution</summary>

~~~
[3,4,1,7,2,6,5,0]
[3,1,4,2,6,5,0,7]
[1,3,2,4,5,0,6,7]
[1,2,3,4,0,5,6,7]
[1,2,3,0,4,5,6,7]
[1,2,0,3,4,5,6,7]
[1,0,2,3,4,5,6,7]
[0,1,2,3,4,5,6,7]
~~~

</details>


`C: [5,4,3,2,1]`

<details>
<summary>Solution</summary>

~~~
[5,4,3,2,1]
[4,3,2,1,5]
[3,2,1,4,5]
[2,1,3,4,5]
[1,2,3,4,5]
~~~

</details>

## Tri Sélection

![Démonstration du tri sélection](https://upload.wikimedia.org/wikipedia/commons/9/94/Selection-Sort-Animation.gif)

`A: [1,5,0,2,7,4,3]`

<details>
<summary>Solution</summary>

~~~
[1,5,0,2,7,4,3]
[0|5,1,2,7,4,3]
[0,1|5,2,7,4,3]
[0,1,2|5,7,4,3]
[0,1,2,3|7,5,4]
[0,1,2,3,4|7,5]
[0,1,2,3,4,5|7]
~~~

</details>


`B: [3,4,1,7,2,6,5,0]`

<details>
<summary>Solution</summary>

~~~
[3,4,1,7,2,6,5,0]
[0|4,3,7,2,6,5,1]
[0,1|4,7,3,6,5,2]
[0,1,2|7,4,6,5,3]
[0,1,2,3|7,6,5,4]
[0,1,2,3,4|7,6,5]
[0,1,2,3,4,5|7,6]
[0,1,2,3,4,5,6|7]
~~~

</details>

`C: [5,4,3,2,1]`

<details>
<summary>Solution</summary>

~~~
[5,4,3,2,1]
[1|5,4,3,2]
[1,2|5,4,3]
[1,2,3|5,4]
[1,2,3,4|5]
~~~

</details>

## Tri Insertion
![Démonstration du tri insertion](https://upload.wikimedia.org/wikipedia/commons/0/0f/Insertion-sort-example-300px.gif)

`A: [1,5,0,2,7,4,3]`

<details>
<summary>Solution</summary>

~~~
[1,5,0,2,7,4,3]
[1|5,0,2,7,4,3]
[1,5|0,2,7,4,3]
[0,1,5|2,7,4,3]
[0,1,2,5|7,4,3]
[0,1,2,5,7|4,3]
[0,1,2,4,5,7|3]
[0,1,2,3,4,5,7]
~~~

</details>

`B: [3,4,1,7,2,6,5,0]`

<details>
<summary>Solution</summary>

~~~
[3,4,1,7,2,6,5,0]
[3|4,1,7,2,6,5,0]
[3,4|1,7,2,6,5,0]
[1,3,4|7,2,6,5,0]
[1,3,4,7|2,6,5,0]
[1,2,3,4,7|6,5,0]
[1,2,3,4,6,7|5,0]
[1,2,3,4,5,6,7|0]
[0,1,2,3,4,5,6,7]
~~~

</details>

`C: [5,4,3,2,1]`

<details>
<summary>Solution</summary>

~~~
[5,4,3,2,1]
[5|4,3,2,1]
[1,5|4,3,2]
[1,2,5|4,3]
[1,2,3,5|4]
[1,2,3,4,5]
~~~

</details>

## Tri Rapide
![Démonstration du tri rapide](https://upload.wikimedia.org/wikipedia/commons/6/6a/Sorting_quicksort_anim.gif)

Ce tri est très complexe, mais beaucoup plus rapide que les autres tris puisqu'il divise les espaces de recherches en deux successivement. Donc je recommande de tracer pas à pas chacun des indices puisque les manipulations sont subtiles.

Il existe plusieurs versions de ce tri, donc utilisé la version présentée dans le cours. Par exemple, la démonstration ci-dessous ne choisit pas ses pivots de la même façon, mais le reste de l'algorithme est pareil.

![Démonstration du tri rapide](https://upload.wikimedia.org/wikipedia/commons/9/9c/Quicksort-example.gif)

Dans les solutions, Les indices indiquent `[debut:pivot:fin]`, Pivot indique la __valeur__ à utiliser comme pivot. J'ai aussi fait un programme de test, à l'aide du code du prof, pour valider les solutions.

[Code pour tester le tri rapide](TestAlgoTri.java)

`A: [1,5,0,2,7,4,3]`

<details>
<summary>Solution</summary>


~~~
[1,5,0,2,7,4,3]      Indices [0:3:6] Pivot = 2
[1,2,0][5,7,4,3]     Indices [0:1:2] Pivot = 2
[1,0][2][5,7,4,3]    Indices [0:0:1] Pivot = 1
[0,1][2][5,7,4,3]    Indices [3:4:6] Pivot = 7
[0,1][2][5,3,4][7]   Indices [3:4:5] Pivot = 3
[0,1][2][3][5,4][7]  Indices [4:5:4] Pivot = 5
[0,1][2][3][4,5][7]
~~~

</details>

`B: [3,4,1,7,2,6,5,0]`

<details>
<summary>Solution</summary>

~~~
[3,4,1,7,2,6,5,0]     Indices [0:3:7] Pivot = 7
[3,4,1,0,2,6,5][7]    Indices [0:3:6] Pivot = 0
[0][4,1,3,2,6,5][7]   Indices [1:3:6] Pivot = 3
[0][2,1,3][4,6,5][7]  Indices [1:2:4] Pivot = 1
[0][1,2][3][4,6,5][7] Indices [4:5:6] Pivot = 6
[0][1,2][3][4,5][6][7]
~~~

</details>

`C: [5,4,3,2,1]`

<details>
<summary>Solution</summary>

~~~
[5,4,3,2,1]     Indices [0:2:4] Pivot = 3
[1,2,3][4,5]    Indices [0:1:2] Pivot = 2
[1,2][3][4,5]
~~~

</details>

