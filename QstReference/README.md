# INF2120 Laboratoires
Par Eric Pietrocupo

## Exercice: Questions concernant les références

### Question 01

Il y a un problème avec ce bout de code, trouvez pourquoi et faites les corrections nécessaires. Allez consulter la javadoc pour la classe `Rectangle` si nécessaire.

~~~
public class SomethingIsWrong {
    public static void main(String[] args) {
        Rectangle myRect;
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
    }
}
~~~

### Question 02

Suite à l'exécution du bout de code suivant, combien existe-t-il de références?
Quels objets sont prêts à se faire détruire par le ramasse-miettes?

~~~
...
String[] students = new String[10];
String studentName = "Peter Parker";
students[0] = studentName;
studentName = null;
...
~~~

SOURCE: https://docs.oracle.com/javase/tutorial/java/javaOO/QandE/objects-questions.html

