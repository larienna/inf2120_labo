package demo5;

public class File<E> {
	protected class Noeud<F> {
		public F element;
		public Noeud<F> suivant;
		
		public Noeud( F element ) {
			this.element = element;
			this.suivant = null;
		}
	}
	
	private Noeud<E> debut = null;
	private Noeud<E> fin = null;
	private int nbElement = 0;

	public File() {
	}
	
	public int taille() {
		return nbElement;
	}
	
	public boolean estVide() {
		return 0 == nbElement;
	}
	
	public E tete() throws FileVide{
		if( null == debut ) {
			throw new FileVide();
		}
		
		return debut.element;
	}
	
	public void enfiler( E element ){
		Noeud<E> nouveau = new Noeud<>( element );
		
		if( null != fin ){
			fin.suivant = nouveau;
		}
		
		fin = nouveau;
		
		if( null == debut ){
			debut = nouveau;
		}
                ++ nbElement;
	}
	
	public void defiler() throws FileVide{
		if( null == debut ) {
			throw new FileVide();
		}
		
		if( fin == debut ){
			fin = null;
		}
		
		debut = debut.suivant;
                -- nbElement;
	}
}