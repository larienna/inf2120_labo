package demo5;

public class Principal {

	public static void main(String[] args) {
		Integer UN = new Integer( 1 );
		Integer DEUX = new Integer( 2 );
		Integer TROIS = new Integer( 3 );
		
		File<Integer> f = new File<>();
		if( ! f.estVide() ) {
			System.out.println( "erreur, la nouvelle file n'est pas vide." );
		}

		

		f.enfiler( DEUX );
		if( f.taille() != 1 ){
			System.out.println( "erreur, il devrait y avoir un element." );
		}
		try {
			if( f.tete() != DEUX ){
				System.out.println( "erreur, le premier element devrait etre 2." );			
			}
		} catch( FileVide e ) {
			System.out.println( "erreur, l'exception ne devrait pas etre lance." );						
		}

	
	
		f.enfiler( UN );
		if( f.taille() != 2 ){
			System.out.println( "erreur, il devrait y avoir deux elements." );
		}
		try {
			if( f.tete() != DEUX ){
				System.out.println( "erreur, le premier element devrait etre 2." );			
			}
		} catch( FileVide e ) {
			System.out.println( "erreur, l'exception ne devrait pas etre lance." );						
		}

	
	
		f.enfiler( TROIS );
		if( f.taille() != 3 ){
			System.out.println( "erreur, il devrait y avoir trois elements." );
		}
		try {
			if( f.tete() != DEUX ){
				System.out.println( "erreur, le premier element devrait etre 2." );			
			}
		} catch( FileVide e ) {
			System.out.println( "erreur, l'exception ne devrait pas etre lance." );						
		}

		
		
		try {
			f.defiler();
			if( f.taille() != 2 ){
				System.out.println( "erreur, il devrait y avoir deux elements." );
			}
			if( f.tete() != UN ){
				System.out.println( "erreur, le premier element devrait etre 1." );			
			}
		} catch( FileVide e ) {
			System.out.println( "erreur, l'exception ne devrait pas etre lance." );						
		}

	
	
		try {
			f.defiler();
			if( f.taille() != 1 ){
				System.out.println( "erreur, il devrait y avoir un element." );
			}
			if( f.tete() != TROIS ){
				System.out.println( "erreur, le premier element devrait etre 3." );			
			}
		} catch( FileVide e ) {
			System.out.println( "erreur, l'exception ne devrait pas etre lance." );						
		}

	
	
		try {
			f.defiler();
			if( f.taille() != 0 ){
				System.out.println( "erreur, il devrait y avoir zero element." );
			}
		} catch( FileVide e ) {
			System.out.println( "erreur, l'exception ne devrait pas etre lance." );						
		}
	}
}
