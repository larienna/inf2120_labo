package labo;

import java.util.ArrayList;
import java.util.Optional;

public class Principal {
    public static <T> PeutEtre<Integer> trouverElement( T[] a_tableau, T a_element ) {
        PeutEtre<Integer> resultat = null;
        int i = 0;

        while( i < a_tableau.length && a_tableau[i].equals( a_element ) ) {
            ++ i;
        }

        if( i < a_tableau.length ) {
            resultat = new QQChose<>( i );
        } else {
            resultat = new Rien<>();
        }

        return resultat;
    }

    public static <T> Optional<Integer> trouverElementV2( T[] a_tableau, T a_element ) {
        Optional<Integer> resultat = null;
        int i = 0;

        while( i < a_tableau.length && a_tableau[i].equals( a_element ) ) {
            ++ i;
        }

        if( i < a_tableau.length ) {
            resultat = Optional.of( i );
        } else {
            resultat = Optional.empty();
        }

        return resultat;
    }

    public static ArrayList<Double> tweens( double depart, double fin, int nbrInterval ) {
        ArrayList<Double> resultat = new ArrayList<>();
        double delta = ( fin - depart ) / nbrInterval;

        resultat.add( depart );
        for( int i = 0; i < nbrInterval; ++ i ) {
            depart += delta;
            resultat.add( depart );
        }

        return resultat;
    }

    public static void main( String [] args ) {
        System.out.println( tweens( 1.0, 3.0, 4 ) );
    }
}
