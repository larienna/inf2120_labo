package inf2120.demo;

public class NonTaxable extends Produit {
    public NonTaxable( String nomProduit, double prix ) {
        super( nomProduit, prix );
    }

    @Override
    public double prix() {
        return prix;
    }
}
