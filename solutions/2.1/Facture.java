package inf2120.demo;

public class Facture {
    private static final int NBR_MAXIMUM = 30;

    private Produit [] produits = new Produit[NBR_MAXIMUM];

    private int nbrProduit = 0;

    /*
    public void ajouterProduitNonTaxable( NonTaxable produit ) {
        if( nbrProduit < NBR_MAXIMUM ) {
            produits[ nbrProduit ] = produit;
            ++ nbrProduit;
        } else {
            System.err.println( "Dépassé le maximum d'éléments pour une facture." );
        }
    }

    public void ajouterProduitTaxeSimple( TaxeSimple produit ) {
        if( nbrProduit < NBR_MAXIMUM ) {
            produits[ nbrProduit ] = produit;
            ++ nbrProduit;
        } else {
            System.err.println( "Dépassé le maximum d'éléments pour une facture." );
        }
    }

    public void ajouterProduitTaxeDouble( TaxeDouble produit ) {
        if( nbrProduit < NBR_MAXIMUM ) {
            produits[ nbrProduit ] = produit;
            ++ nbrProduit;
        } else {
            System.err.println( "Dépassé le maximum d'éléments pour une facture." );
        }
    }
    */

    public void ajouterProduit( Produit produit ) {
        if( nbrProduit < NBR_MAXIMUM ) {
            produits[ nbrProduit ] = produit;
            ++ nbrProduit;
        } else {
            System.err.println( "Dépassé le maximum d'éléments pour une facture." );
        }
    }

    public double prixTotal() {
        double somme = 0.0;

        for( int i = 0; i < nbrProduit; ++ i ) {
            somme += produits[ i ].prix();
        }

        return somme;
    }
}
