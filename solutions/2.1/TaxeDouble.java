package inf2120.demo;

public class TaxeDouble extends Produit {
    public TaxeDouble( String nomProduit, double prix ) {
        super( nomProduit, prix );
    }

    @Override
    public double prix() {
        return prix * Constantes.MULTIPLICATEUR_DOUBLE;
    }
}
