package inf2120.demo;

public class Constantes {
    public static final double TPS = 0.05;
    public static final double TVQ = 0.09975;

    public static final double MULTIPLICATEUR_SIMPLE = 1.0 + TPS;
    public static final double MULTIPLICATEUR_DOUBLE = ( 1.0 + TVQ ) * MULTIPLICATEUR_SIMPLE;
}
