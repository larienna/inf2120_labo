package inf2120.demo;

public class TaxeSimple extends Produit{
    public TaxeSimple( String nomProduit, double prix ) {
        super( nomProduit, prix );
    }

    @Override
    public double prix() {
        return prix * Constantes.MULTIPLICATEUR_SIMPLE;
    }
}
