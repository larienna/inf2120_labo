package inf2120.demo;

public class Produit {
    protected String nomProduit;
    protected double prix;

    public Produit( String nomProduit, double prix ) {
        this.nomProduit = nomProduit;
        this.prix = prix;
    }

    public double prix() {
        return 0;
    }

    public String getNomProduit() {
        return nomProduit;
    }

    public void setNomProduit(String nomProduit) {
        this.nomProduit = nomProduit;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }
}
