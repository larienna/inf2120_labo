package inf2120.s213g20;

import java.util.*;

// Écrire le code pour une File : héritage.

public class Principal {
    public static < E extends Comparable< E > >
    int rechercheBinaire( E [] tab, E cible ) {
        int debut = 0;
        int fin = tab.length - 1;
        while( debut < fin ) {
            int milieu = ( debut + fin ) / 2;
            if( cible.compareTo( tab[ milieu ] ) <= 0 ) {
                fin = milieu;
            } else {
                debut = milieu + 1;
            }
        }
        if( tab[debut].compareTo( cible ) != 0 ) {
            debut = -1;
        }
        return debut;
    }

    public static < E extends Comparable< E > >
    int rechercheBinaireR( E [] tab, E cible, int debut, int fin ) {
        int resultat;
        if( debut < fin ) {
            int milieu = ( debut + fin ) / 2;
            if( cible.compareTo( tab[ milieu ] ) <= 0 ) {
                resultat = rechercheBinaireR( tab, cible, debut, milieu );
            } else {
                resultat = rechercheBinaireR( tab, cible, milieu + 1, fin );
            }
        }
        else if( tab[debut].compareTo( cible ) == 0 ) {
            resultat = debut;
        } else {
            resultat = -1;
        }
        return resultat;
    }

    public static void main( String [] argv ) {
        Fraction [] f1 = {
                new Fraction( 1, 2 ),
        };
        Fraction [] f2 = {
                new Fraction( 1, 2 ),
                new Fraction( 2, 2 ),
        };
        Fraction [] f3 = {
                new Fraction( 1, 2 ),
                new Fraction( 2, 2 ),
                new Fraction( 3, 2 ),
        };
        Fraction [] f16 = {
                new Fraction( 1, 2 ),
                new Fraction( 2, 2 ),
                new Fraction( 3, 2 ),
                new Fraction( 4, 2 ),
                new Fraction( 5, 2 ),
                new Fraction( 6, 2 ),
                new Fraction( 7, 2 ),
                new Fraction( 8, 2 ),
                new Fraction( 9, 2 ),
                new Fraction( 10, 2 ),
                new Fraction( 11, 2 ),
                new Fraction( 12, 2 ),
                new Fraction( 13, 2 ),
                new Fraction( 14, 2 ),
                new Fraction( 15, 2 ),
                new Fraction( 16, 2 ),
        };

        Arrays.binarySearch( f16, new Fraction( 4, 2 ) );
    }
}
