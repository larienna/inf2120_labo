package labo;

import java.util.ArrayList;

public class Principal2 {
    public static <N extends Nombre<N> > Nombre<N> somme(ArrayList<N> tableau ) {
        N somme = null;

        if( ! tableau.isEmpty() ) {
            somme = tableau.get( 0 );

            for( int i = 1; i < tableau.size(); ++ i ) {
                somme = somme.add( tableau.get( i ) );
            }
        }

        return somme;
    }

    public static void main( String [] argv ) {
        ArrayList<NDouble> t1 = new ArrayList<>();
        t1.add( new NDouble( 1.0 ) );
        t1.add( new NDouble( 2.45 ) );
        t1.add( new NDouble( -1.2 ) );

        System.out.println( somme( t1 ) );

        ArrayList<Fraction> t2 = new ArrayList<>();
        t2.add( new Fraction( 2, 5 ) );
        t2.add( new Fraction( 1, 3 ) );
        t2.add( new Fraction( 5, 2 ) );
        t2.add( new Fraction( 1, 10 ) );

        System.out.println( somme( t2 ) );
    }
}
