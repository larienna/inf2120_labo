package INF2120.demo;

public class CylindreDroit extends Forme3D {
    protected Forme2D base;
    protected double hauteur;

    public CylindreDroit( Forme2D base, double hauteur ) {
        this.base = base;
        this.hauteur = hauteur;
    }

    @Override
    public double volume() {
        return base.aire() * hauteur;
    }

    @Override
    public String toString() {
        String r = "<cylindre_droit hauteur=\"" + hauteur + "\">\n";
        r += base + "\n";
        r += "</cylindre_droit>";
        return r;
    }
}
