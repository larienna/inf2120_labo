package INF2120.demo;

public class Cercle extends Forme2D {
    protected double rayon = 0.0;

    public Cercle( double rayon ) {
        this.rayon = rayon;
    }

    @Override
    public double aire() {
        return Math.PI * rayon * rayon;
    }

    @Override
    public String toString() {
        return "<cercle rayon=\"" + rayon + "\"/>";
    }
}
