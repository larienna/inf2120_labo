package INF2120.demo;

public class Sphere extends Forme3D {
    protected double rayon;
    private static final double K_PI = 4.0 * Math.PI / 3.0;

    public Sphere( double volume ) {
        this.rayon = volume;
    }

    @Override
    public double volume() {
        return K_PI * rayon * rayon * rayon;
    }

    @Override
    public String toString() {
        return "<sphere rayon=\"" + rayon + "\"/>";
    }
}
