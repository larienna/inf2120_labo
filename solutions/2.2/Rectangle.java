package INF2120.demo;

public class Rectangle extends Forme2D {
    protected double hauteur = 0.0;
    protected double largeur = 0.0;

    public Rectangle( double hauteur, double largeur ) {
        this.hauteur = hauteur;
        this.largeur = largeur;
    }

    @Override
    public double aire() {
        return hauteur * largeur;
    }

    @Override
    public String toString() {
        return "<rectangle hauteur=\"" + hauteur +
            "\" largeur=\"" + largeur + "\"/>";
    }
}
