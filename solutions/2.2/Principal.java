package INF2120.demo;

public class Principal {
    public static void main( String [] argv ) {
        Forme3D [] tab = {
            new Sphere( 2.0 ),
            new CylindreDroit( new Cercle( 3.0 ), 2.0 ),
            new CylindreDroit( new Rectangle( 5.0, 2.3 ), 1.5 )
        };

        for( int i = 0; i < tab.length; ++ i ) {
            System.out.println(
                tab[i] + "\n  volume = " +
                tab[i].volume() + "\n"
            );
        }
    }
}
