package inf2120;

import inf2120.s213g20.Fraction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Supplier;

public class Principal {
    public static final int MAX = 100;
    public static final int NB_VALEURS = 1_000_000;

    public static void tester(Supplier< Integer > s ) {
        int [] tab = new int[MAX];

        for( int i = 0; i < NB_VALEURS; ++ i ) {
            ++ tab[ s.get() ];
        }
        System.out.println( Arrays.toString( tab ) );
    }

    public static void n5_2_1() {
        Function<ArrayList< String >, String > c =
                ( a ) -> {
            String r = "";
            if( a.size() != 0 ) {
                r = a.get( 0 );
                for( int i = 1; i < a.size(); ++ i ) {
                    r += "," + a.get( i );
                }
            }
            return r;
        };

        ArrayList<String> x = new ArrayList<>();
        x.add( "allo" );
        x.add( "comment" );
        x.add( "ca" );
        x.add( "va" );

        System.out.println( c.apply( x ) );
    }

    public static void n5_2_2() {
        ArrayList<Fraction> fs = new ArrayList<>();
        fs.add( new Fraction( 1, 2 ) );
        fs.add( new Fraction( 3, 4 ) );

        fs.forEach( ( f ) -> f.setNum( 2 * f.getNum() ) );

        System.out.println( fs );
    }

    public static void n5_2_3() {
        ArrayList<Integer> a = new ArrayList<>();
        MRand r = new MRand( 10 );
        for( int i = 0; i < 20; ++ i ) {
            a.add( r.get() );
        }
        a.removeIf( (x) -> x < 4 );

        System.out.println( a );
    }

    public static void main( String [] argv ) {
        MRand random = new MRand( MAX );

        tester( random );

        n5_2_1();
        n5_2_2();
        n5_2_3();
    }
}
