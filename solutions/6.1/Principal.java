package inf2120;


import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class Principal {

    public static <E> Deque<E> inverser( Deque< E > a ) {
        Deque<E> s = new ArrayDeque<>();

        while( !a.isEmpty() ) {
            s.push( a.pop() );
        }

        return s;
    }

    public static <E> void inverser2( Deque< E > a ) {
        Queue<E> f = new ArrayDeque<>();

        while( ! a.isEmpty() ) {
            f.add( a.pop() );
        }
        while( ! f.isEmpty() ) {
            a.push( f.remove() );
        }
    }

    public static boolean parentCorrect( String s ) {
        Deque< Character > p = new ArrayDeque<>();
        boolean resultat = true;

        for( int i = 0; i < s.length(); ++ i ) {
            char c = s.charAt( i );

            switch( c ) {
                case '(', '[', '{' -> p.push( c );
                case ')' -> resultat &= p.pop().equals( '(' );
                case ']' -> resultat &= p.pop().equals( '[' );
                case '}' -> resultat &= p.pop().equals( '{' );
                default -> {}
            }
        }
        return resultat;
    }

    public static void main( String [] argv ) {
        System.out.println( parentCorrect( "(dsf[gds]g[dfg{fdgf}f(g)re]g)d[s]" ) );
    }
}
