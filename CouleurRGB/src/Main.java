public class Main {

    public static void main(String[] args) {
        ex01_couleurs();
    }

    /**
     * Extension de la classe couleur pour supporter les differents formats RGB
     */
    public static void ex01_couleurs ( ) {
        Couleur c1 = new Couleur( 1, 200, 6 );
        Couleur c2 = c1;
        System.out.println( c2 );
        c2.setRouge( 100 );
        System.out.println( c1 );
        c1.blanchir();
        System.out.println( c1 );
        System.out.println( c2 );

        //exercices avances
        System.out.println( c1 );
        System.out.println ( "RGB332 : " + c1.getRgb8() );
        System.out.println ( "RGB888 : " + c1.getRgb24() );
        System.out.println ( "RGB565 : " + c1.getRgb16() );

        System.out.println( c1 );
        System.out.println ( "RGB565 : " + c1.getRgbXyz( 5,6,5 ) );
        System.out.println ( "RGB565 : " + c1.getFormatRgb( "RGB565") );
        System.out.println ( "RGB308 : " + c1.getFormatRgb( "RGB308") );
        System.out.println ( "RGB746 : " + c1.getFormatRgb( "RGB746") );

        Couleur c3 = new Couleur( 255, 255, 255 );
        System.out.println( c3 );
        System.out.println ( "RGB332 : " + c3.getRgb8() );
        System.out.println ( "RGB888 : " + c3.getRgb24() );
        System.out.println ( "RGB565 : " + c3.getRgb16() );

        Couleur c4 = new Couleur( 254, 254, 254 );
        System.out.println( c4 );
        System.out.println ( "RGB332 : " + c4.getRgb8() );
        System.out.println ( "RGB888 : " + c4.getRgb24() );
        System.out.println ( "RGB565 : " + c4.getRgb16() );
    }
}
