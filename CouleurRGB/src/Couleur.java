public class Couleur {
    public static final int VALEUR_COMPOSANTE_MIN = 0;
    public static final int VALEUR_COMPOSANTE_MAX = 255;
    public static final int NB_BIT_MIN = 1;
    public static final int NB_BIT_MAX = 10;

    private int rouge = 0;
    private int vert = 0;
    private int bleu = 0;

    public Couleur() {}

    public Couleur( int rouge, int vert, int bleu ) {
        setRouge( rouge );
        setVert( vert );
        setBleu( bleu );
    }

    public int getRouge() {
        return rouge;
    }

    public int getVert() {
        return vert;
    }

    public int getBleu() {
        return bleu;
    }

    public void setRouge(int rouge) {
        if( rouge < VALEUR_COMPOSANTE_MIN || VALEUR_COMPOSANTE_MAX < rouge ) {
            System.err.println(
                    "Composante rouge doit etre entre " + VALEUR_COMPOSANTE_MIN +
                            " et " + VALEUR_COMPOSANTE_MAX + "");

        } else this.rouge = rouge;
    }

    public void setVert(int vert) {
        if( vert < VALEUR_COMPOSANTE_MIN || VALEUR_COMPOSANTE_MAX < vert ) {
            System.err.println(
                    "Composante verte doit etre entre " + VALEUR_COMPOSANTE_MIN +
                            " et " + VALEUR_COMPOSANTE_MAX + "");
        } else this.vert = vert;
    }

    public void setBleu(int bleu) {
        if( bleu < VALEUR_COMPOSANTE_MIN || VALEUR_COMPOSANTE_MAX < bleu ) {
            System.err.println(
                    "Composante bleu doit etre entre " + VALEUR_COMPOSANTE_MIN +
                            " et " + VALEUR_COMPOSANTE_MAX + "");
        } else this.bleu = bleu;
    }

    public void blanchir() {
        rouge = (rouge + 255) / 2;
        vert = (vert + 255) / 2;
        bleu = (bleu + 255) / 2;
    }

    @Override
    public String toString() {
        return String.format("(R:%d,G:%d,B:%d)", rouge, vert, bleu);
    }

    //__________________________ Exercices avances _________________________

    /**
     * @return un entier au format RGB332
     */
    public int getRgb8 (){
        int rgb = convertir ( bleu, 8, 2);
        rgb += convertir ( vert, 8, 3) << 2;
        rgb += convertir ( rouge, 8, 3) << 5;
        return rgb;
    }

    /**
     *
     * @return un entier au format RGB565
     */
    public int getRgb16(){
        int rgb = convertir ( bleu, 8, 5);
        rgb += convertir ( vert, 8, 6) << 5;
        rgb += convertir ( rouge, 8, 5) << 11;
        return rgb;
    }

    /**
     * Cette solution n'utilise pas les operateurs de shift de bits
     * @return un entier au format RGB888
     */
    public int getRgb24(){
        int rgb = bleu;
        rgb += vert * 256;
        rgb += rouge * 256 * 256;
        return rgb;
    }

    /**
     * Cette fonction permet de convertir une composante de couleur (rouge, vert ou bleu).
     * Les composantes de 0-255 ont 8 bits.
     * Cette fonction permet de reduire ou agrandir l'espace de couleur
     * @param composante de couleur a reduire ou augmenter.
     * @param nbBitSrc nb de bit de la composante passe en parametre
     * @param nbBitDst nb de bit de la composante a retourner.
     * @return la nouvelle composante convertie
     */
    private int convertir ( int composante, int nbBitSrc, int nbBitDst ){
        //on multiplie avant de diviser pour eviter les nombres fractionnaires.
        return (int) ( composante * Math.pow ( 2, nbBitDst ) / Math.pow ( 2, nbBitSrc ));
    }

    /**
     * Calcule un format de couleur sur mesure de 1 a 10 bits maximum par composante.
     * La fonction valide que le nombre de bits demeure entre 1 et 10
     * Cette fonction est plus generique et flexible que rgb8,16,24
     * @param bitsRouge Nombre de bits pour la composante rouge
     * @param bitsVert Nombre de bits pour la composante vert
     * @param bitsBleu Nombre de bits pour la composante bleu
     * @return Un entier encodant les 3 composantes
     */
    public int getRgbXyz ( int bitsRouge, int bitsVert, int bitsBleu ){
        if( !(bitsRouge >= NB_BIT_MIN &&
                bitsVert >= NB_BIT_MIN &&
                bitsBleu >= NB_BIT_MIN &&
                bitsRouge <= NB_BIT_MAX &&
                bitsVert <= NB_BIT_MAX &&
                bitsBleu <= NB_BIT_MAX) ) {
            System.err.println(
                    "Le nombre de bits doit etre entre " + NB_BIT_MIN +
                            " et " + NB_BIT_MAX + "");
        }

        int rgb = convertir ( bleu, 8, bitsBleu);
        rgb += convertir ( vert, 8, bitsVert) << bitsBleu;
        rgb += convertir ( rouge, 8, bitsRouge) << bitsBleu + bitsVert;
        return rgb;
    }

    /**
     * Prend un format RGB sous forme de chaine de caractere, le caractere 0 = 10 bits
     * Les formats ont la nomenclature suivante: 'RGB234'
     *
     * RGB est une chaine constante
     * 2 = nombre de bits pour la composante rouge
     * 3 = nombre de bits pour la composante verte
     * 4 = nombre de bits pour la composante bleu
     *
     * le nombre 0 indique une composante de 10 bits
     *
     * @param format chaine de formatage de 6 caracteres
     * @return un entier du format specifie
     */
    public int getFormatRgb ( String format ) {
        if ( format.length() != 6) {
            System.err.println( "La taille du format doit être de 6 caractères");
            return 0;
        }

        int bitsRouge = format.charAt(3) - '0';
        int bitsVert = format.charAt(4) - '0';
        int bitsBleu = format.charAt(5) - '0';
        if ( bitsRouge == 0) bitsRouge = 10;
        if ( bitsVert == 0) bitsVert = 10;
        if ( bitsBleu == 0) bitsBleu = 10;

        return getRgbXyz( bitsRouge, bitsVert, bitsBleu);
    }

    public void setRgb8 ( int couleur ){

    }

    public void setRgb16 ( int couleur ){

    }

    public void setRgb24 ( int couleur ){

    }

    public void setRgbXyz ( int bitsRouge, int bitsVert, int bitsBleu, int couleur ){

    }

    public void setFormatRgb ( String format, int couleur ){

    }



}
