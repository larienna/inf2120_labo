# INF2120 Laboratoires
Par Eric Pietrocupo

## Exercice: Encodage des couleurs au format RGB.

Cet exercice semble être plus complexe que prévu après avoir. Vous pouvez essayer de compléter cet exercice si vous savez compter en binaire. Par exemple:

~~~
 00110
+10000
=10110
~~~

Si l'équation ci dessus fait du sens pour vous, vous êtes en mesure de compléter cet exercice.


### 01.A: Formats de couleur RGB

Réutiliser l'exercice 1.1, la classe Couleur, pour ajouter des nouvelles fonctionnalités.
Convertissez les composantes rouge, vert et bleu en un seul entier de divers format RGB.
Voici les signatures à implémenter:

~~~
    public int getRgb8 () // RGB332
    public int getRgb16 () // RGB565
    public int getRgb24 () // RGB888
~~~

Les formats RGB juxtaposent les composantes dans un même entier en leur attribuant une puissance différente. Pour le format RGB888, les composantes sont disposées ainsi:

~~~
------------------------------------------
| rouge 0-255 | vert 0-255 | bleu 0-255 |
------------------------------------------
~~~

On peut déplacer les composantes vers la gauche en multipliant par 256 ou avec l'opérateur `<< 8` pour faire un déplacement de 8 bits vers la gauche. Le nombre de déplacement variera selon le nombre de bits de chaque composante.


Vous aurez besoin d'une fonction pour convertir chacune des composantes RGB en quelque chose de plus petit ou plus grand. Les formules nécessaires sont les suivantes:

~~~
Taille maximale d'une composante: 2 exposant nbDeBits
Formule de conversion: (composante / taille source) * taille de la destination
~~~

Faites attention aux conversions entre entier et doubles pour ne pas perdre de la précision. Arrondissez à l'entier le plus proche. Voici la signature de la fonction:

~~~
    /**
     * Cette fonction permet de convertir une composante de couleur (rouge, vert ou bleu).
     * Les composantes de 0-255 ont 8 bits.
     * Cette fonction permet de reduire ou agrandir l'espace de couleur
     * @param composante de couleur a reduire ou augmenter.
     * @param nbBitSrc nb de bit de la composante passe en parametre
     * @param nbBitDst nb de bit de la composante a retourner.
     * @return la nouvelle composante convertie
     */
    private int convertir ( int composante, int nbBitSrc, int nbBitDst )
~~~

Voici un exemple de résultat d'exécution:

~~~
(R:177,G:227,B:130)

RGB332 : 222
RGB888 : 11658114
RGB565 : 46896
~~~

### 01.B: Format maison de composante RGB

On aimerait obtenir des formats de composantes de couleur maisons. Donc le nombre de bits de chaque composante est variables.

Faites d'abord une fonction qui convertit la couleur en un entier en spécifiant le nombre de bits de chaque composante. La fonction doit s'assurer que le nombre de bits se situe entre 1 et 10.

~~~
 /**
     * Calcule un format de couleur sur mesure de 1 a 10 bits maximum par composante.
     * La fonction valide que le nombre de bits demeure entre 1 et 10
     * Cette fonction est plus generique et flexible que rgb8,16,24
     * @param bitsRouge Nombre de bits pour la composante rouge
     * @param bitsVert Nombre de bits pour la composante vert
     * @param bitsBleu Nombre de bits pour la composante bleu
     * @return Un entier encodant les 3 composantes
     */
    public int getRgbXyz ( int bitsRouge, int bitsVert, int bitsBleu )
~~~

Faites une autre fonction qui lit une chaîne de caractère, contenant le nom du format, et qui extrait le nombre de bits de celle-ci. La taille de la chaîne doit être validé.

~~~
/**
     * Prend un format RGB sous forme de chaine de caractere, le caractere 0 = 10 bits
     * Les formats ont la nomenclature suivante: 'RGB234'
     *
     * RGB est une chaine constante
     * 2 = nombre de bits pour la composante rouge
     * 3 = nombre de bits pour la composante verte
     * 4 = nombre de bits pour la composante bleu
     *
     * le nombre 0 indique une composante de 10 bits
     *
     * @param format chaine de formatage de 6 caracteres
     * @return un entier du format specifie
     */
    public int getFormatRgb ( String format )
~~~

Voici un exemple d'utilisation

~~~
Couleur c1 = new Couleur( 177, 227, 130 );

System.out.println( c1 );
System.out.println ( "RGB565 : " + c1.getRgbXyz( 5,6,5 ) );
System.out.println ( "RGB565 : " + c1.getFormatRgb( "RGB565") );
System.out.println ( "RGB308 : " + c1.getFormatRgb( "RGB308") );
System.out.println ( "RGB746 : " + c1.getFormatRgb( "RGB746") );
~~~

et son résultat

~~~
(R:177,G:227,B:130)
RGB565 : 46896
RGB565 : 46896
RGB308 : 1805442
RGB746 : 91040
~~~

### 01.C: Setters de format RGB

Pour compléter le tout, cette dernière partie est beaucoup plus complexe et peut être ignorée. Il s'agit de faire des setters à partir d'un entier RGB. Cela requiert l'utilisation de masques avec les opérateurs bits à bits ( & (AND), | (OR), ^ (XOR) ) pour extraire chacune des composantes de l'entier. Par exemple, on peut extraire la composante verte de cette façon pour un RGB333:

~~~
        101 011 100
AND     000 111 000
=       000 011 000
SHIFT 3 000 000 011
~~~

Les signatures de fonctions seraient les suivantes:

~~~
public void setRgb8 ( int couleur )
public void setRgb16 ( int couleur )
public void setRgb24 ( int couleur )
public void setRgbXyz ( int bitsRouge, int bitsVert, int bitsBleu, int couleur )
public void setFormatRgb ( String format, int couleur )
~~~
